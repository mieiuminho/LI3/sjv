package common;

public interface IParStringNum {

    void setString(String string);

    String getString();

    Number getNumber();
}
