package model.filial;

import java.io.Serializable;
import java.util.*;

public class ClienteProdutos implements Serializable {
    private String codigoCliente;

    private Map<String, InfoProduto> produtos;

    private int numCompras; // linhas de venda

    public ClienteProdutos(String codigoCliente) {
        this.codigoCliente = codigoCliente;
        this.produtos = new HashMap<>();
        this.numCompras = 0;
    }


    public ClienteProdutos(ClienteProdutos clienteprodutos) {
        this.codigoCliente = clienteprodutos.getCodigoCliente();
        this.numCompras = clienteprodutos.getNumCompras();
        for (InfoProduto infprd : clienteprodutos.getProdutos()) {
            this.produtos.put(infprd.getCodigoProduto(), infprd);
        }
    }

    public String getCodigoCliente() {
        return this.codigoCliente;
    }

    public List<InfoProduto> getProdutos() {
        List<InfoProduto> result = new ArrayList<>();

        for (InfoProduto infprd : this.produtos.values()) {
            result.add(infprd.clone());
        }

        return result;
    }

    public int getTotalComprasProduto(String codigoProduto) {
        return this.produtos.get(codigoProduto).getQuantidadeTotal();
    }

    public double getTotalFaturadoProduto(String codigoProduto) {
        if (this.produtos.containsKey(codigoProduto))
            return this.produtos.get(codigoProduto).getFaturacaoTotal();
        else return 0;
    }

    public int getNumCompras() {
        return this.numCompras;
    }

    public void update(String codigoProduto, String tipo, int quantidade, double facturacao) {
        if (produtos.containsKey(codigoProduto)) {
            produtos.get(codigoProduto).update(tipo, quantidade, facturacao);
        } else {
            InfoProduto infoproduto = new InfoProduto(codigoProduto, tipo, quantidade, facturacao);
            this.produtos.put(codigoProduto, infoproduto);
        }
        this.numCompras++;
    }

    public double getTotalFaturado() {
        double result = 0;

        for (InfoProduto infprd : this.produtos.values()) {
            result += infprd.getFaturacaoTotal();
        }

        return result;
    }

    public boolean comprouProduto(String codigoProduto) {
        boolean ret = false;
        Iterator<String> it = this.produtos.keySet().iterator();
        while (it.hasNext() && !ret) {
            String codigoIp = it.next();
            if (codigoIp.equals(codigoProduto)) ret = true;
        }
        return ret;
    }

    @Override
    public ClienteProdutos clone() {
        return new ClienteProdutos(this);
    }
}
