package unity;

import model.GestVendasModel;
import model.IGestVendasModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestFilial {
    private IGestVendasModel model;

    @Before
    public void setUP() {
        this.model = new GestVendasModel();
        model.createData("target/test-classes/Clientes_Teste.txt", "target/test-classes/Produtos_Teste.txt", "target/test-classes/Vendas_Teste.txt");
    }

    @Test
    public void getNumFiliaisTest() {
        int result = model.getNumFiliais();
        Assert.assertEquals(3, result);
    }
}
