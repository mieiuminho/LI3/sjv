package model;

import java.util.Set;

public interface ICatClientes {

    /**
     * Método que permite obter o número de clientes
     *
     * @return Número de clientes
     */
    int getTotal();

    /**
     * Método que permite obter o número de clientes envolvidos em transações
     *
     * @return Número de clientes envolvidos em transações.
     */
    int getUsados();

    /**
     * Método que permite obter o número de clientes não envolvidos em transações.
     *
     * @return Número de clientes não envolvidos em transações.
     */
    int getNaoUsados();

    /**
     * Método que permite obter os clientes registados.
     *
     * @return `Set` que contém os clientes registados.
     */
    Set<ICliente> getClientes();

    /**
     * Método que permite inserir um cliente na estrutura de clientes.
     *
     * @param cliente Cliente a inserir.
     */
    void insereCliente(ICliente cliente);

    /**
     * Método que permite determinar se um cliente se encontra registado ou não.
     *
     * @param cliente Cliente a averiguar.
     * @return `true` caso o cliente argumento esteja registado ou `false` caso contrário.
     */
    boolean existeCliente(ICliente cliente);
}
