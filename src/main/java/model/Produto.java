package model;

import java.io.Serializable;

public class Produto implements IProduto, Comparable<Produto>, Serializable {

    private String codigo;

    /**
     * Construtor parametrizado.
     *
     * @param codigo Código do produto.
     */
    public Produto(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Construtor parametrizado.
     *
     * @param produto Produto.
     */
    public Produto(Produto produto) {
        this.codigo = produto.getCodigo();
    }

    /**
     * Método que permite obter o código do produto.
     *
     * @return String com o código do produto.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Método que permite definir o código do produto.
     *
     * @param codigo String como o código do produto.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Método que permite obter a cópia de um objeto de um classe que satisfaça a interface `IProduto`
     *
     * @return Cópia do objeto
     */
    @Override
    public IProduto clone() {
        return new Produto(this);
    }

    /**
     * Método que permite determinar se dois objetos são equivalentes.
     *
     * @param o Objeto a comparar.
     * @return `true` caso afirmativo ou `false` caso contrário.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Produto produto = (Produto) o;

        return this.codigo != null ? this.codigo.equals(produto.getCodigo()) : produto.getCodigo() == null;
    }

    /**
     * Método que permite obter o `hashCode`
     *
     * @return `hashCode`
     */
    @Override
    public int hashCode() {
        return getCodigo() != null ? getCodigo().hashCode() : 0;
    }

    /**
     * Método que permite obter uma representação de um objeto do tipo `Produto`.
     *
     * @return String com a representação textual do objeto.
     */
    @Override
    public String toString() {
        return this.codigo;
    }

    /**
     * Método que permite comparar dois objetos do tipo `Produto` (ordem natural).
     *
     * @param produto `Produto` a comparar.
     * @return Um inteiro menor que zero caso o objeto a que alicamos o método venha antes do objeto argumento na ordem
     * natural, um inteiro maior que zero caso o objeto a que aplicamos o método venha depois do objeto argumento na
     * ordem natural ou zero caso ocupem a mesma posição.
     */
    @Override
    public int compareTo(Produto produto) {
        return this.codigo.compareTo(produto.getCodigo());
    }
}
