package model;

import common.IParStringNum;

import java.io.Serializable;

public class ParProdutoFaturacao implements Serializable, IParStringNum {
    private String codigoProduto;
    private double faturacao;

    public ParProdutoFaturacao(String codigoProduto, double faturacao) {
        this.codigoProduto = codigoProduto;
        this.faturacao = faturacao;
    }

    @Override
    public void setString(String string) {
        this.codigoProduto = string;
    }

    @Override
    public String getString() {
        return this.codigoProduto;
    }

    @Override
    public Number getNumber() {
        return this.faturacao;
    }
}
