package view;

import common.IParStringNum;

import java.util.Collection;
import java.util.List;

public interface IGestVendasView {

    int menu(String title, String[] options);

    void displayList(Collection<String> lines, String title);

    void displayTable(Collection<IParStringNum> lines, String title, List<String> fields);

    void displayQuickTable(Collection<IParStringNum> lines, String title, List<String> fields);
}
