package view;

import static java.lang.System.out;

public class Terminal {

    public static final int WINDOW_WIDTH = 208;
    public static final int TEXT_MAX_WIDTH = 90;

    public static void clear() {
        out.print("\033[H\033[2J");
        out.flush();
    }

    public static void error(String error) {
        out.println(Color.ANSI_RED + "\nErro: " + Color.ANSI_RESET + error);
    }

    public static void info(String info) {
        out.println(Color.ANSI_BLUE + "\nInfo: " + Color.ANSI_RESET + info);
    }
}
