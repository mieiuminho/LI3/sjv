package controller;

import common.Crono;
import common.IParStringNum;
import common.ParStringNum;
import exceptions.InvalidCliente;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;
import model.Constantes;
import model.IGestVendasModel;
import org.apache.commons.lang3.StringUtils;
import view.GestVendasView;
import view.IGestVendasView;
import view.Input;
import view.Terminal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.System.out;

public class GestVendasController implements IGestVendasController {
    private IGestVendasModel model;
    private IGestVendasView view;

    public GestVendasController(IGestVendasModel model, IGestVendasView view) {
        this.model = model;
        this.view = view;
    }

    public void run() {
        int option = this.view.menu("Menu Inicial", GestVendasView.opcoesMenuPrincipal);

        switch (option) {
            case 1: // Ler dados
                this.leitura();
                this.run();
                break;
            case 2: // Gravar dados
                try {
                    Crono.start();
                    this.model.save();
                    Crono.stop();
                    out.println(Crono.toShow());
                } catch (IOException e) {
                    out.println("Não foi possível gravar os dados: " + e.getMessage());
                }
                Input.lerEnter();
                this.run();
                break;
            case 3: // Carregar dados
                try {
                    Crono.start();
                    IGestVendasModel loaded = this.model.load();
                    this.model = loaded;
                    Crono.stop();
                    out.println(Crono.toShow());
                } catch (ClassNotFoundException | IOException e) {
                    out.println("Não foi possível carregar os dados: " + e.getMessage());
                }
                Input.lerEnter();
                this.run();
                break;
            case 4: // Estatisticas
                if (this.model.getUltimoFicheiroLido() != null)
                    this.estatisticas();
                else {
                    Terminal.error("Falta ler os dados!");
                    Input.lerEnter();
                    this.run();
                }
                break;
            case 5: // Queries
                if (this.model.getUltimoFicheiroLido() != null)
                    this.queries();
                else {
                    Terminal.error("Falta ler os dados!");
                    Input.lerEnter();
                    this.run();
                }
                break;
            case 0: // Sair
                System.exit(0);
        }
    }

    public void leitura() {
        String fichprodutos, fichclientes, fichvendas;

        out.print("Ficheiro de Clientes (ENTER para default): ");
        fichclientes = Input.lerString().trim();
        out.print("Ficheiro de Produtos (ENTER para default): ");
        fichprodutos = Input.lerString().trim();
        out.print("Ficheiro de Vendas (ENTER para default): ");
        fichvendas = Input.lerString().trim();

        if (fichclientes.isEmpty()) fichclientes = Constantes.FILE_CLIENTES;
        if (fichprodutos.isEmpty()) fichprodutos = Constantes.FILE_PRODUTOS;
        if (fichvendas.isEmpty()) fichvendas = Constantes.FILE_VENDAS;

        Crono.start();
        this.model.createData(fichclientes, fichprodutos, fichvendas);
        Crono.stop();

        out.println(Crono.toShow());
        Input.lerEnter();
    }

    public void estatisticas() {
        int option = this.view.menu("Consultas Estatísticas", GestVendasView.opcoesMenuEstatisticas);

        switch (option) {
            case 1: // Estatísticas Ficheiro
                this.check1();
                break;
            case 2: // Estatísticas Gerais
                this.check2();
                break;
            case 0: // Sair
                this.run();
                break;
        }

        this.estatisticas();
    }

    public void queries() {
        int option = this.view.menu("Consultas Interativas", GestVendasView.opcoesMenuQueries);

        switch (option) {
            case 1:
                query1();
                break;
            case 2:
                query2();
                break;
            case 3:
                query3();
                break;
            case 4:
                query4();
                break;
            case 5:
                query5();
                break;
            case 6:
                query6();
                break;
            case 7:
                query7();
                break;
            case 8:
                query8();
                break;
            case 9:
                query9();
                break;
            case 10:
                query10();
                break;
            case 0: // Sair
                this.run();
                break;
        }

        this.queries();
    }

    public void check1() {
        out.println("Nome do último ficheiro lido: " + this.model.getUltimoFicheiroLido());
        out.println("Número total de registos de vendas errados: " + this.model.getNrVendasErradas());
        out.println("Número total de produtos: " + this.model.getTotalProdutos());
        out.println("Número de produtos diferentes comprados: " + (this.model.getTotalProdutos() - this.model.getTotalProdutosNaoComprados()));
        out.println("Número de produtos não comprados: " + this.model.getTotalProdutosNaoComprados());
        out.println("Número total de Clientes: " + this.model.getTotalClientes());
        out.println("Número total de Clientes que efetuaram compras: " + this.model.getTotalClientesCompraram());
        out.println("Número total de Clientes que não efetuaram compras: " + (this.model.getTotalClientes() - this.model.getTotalClientesCompraram()));

        Input.lerEnter();
    }

    public void check2() {
        try {
            for (int m = 1; m <= Constantes.N_MESES; m++) {
                out.println("Total de vendas no Mês " + m + ": " + this.model.getTotalVendasMes(m));
            }
            out.println("\n");
            for (int f = 1; f <= Constantes.N_FILIAIS; f++) {
                out.println("Filial " + f + " : ");
                for (int m = 1; m <= Constantes.N_MESES; m++) {
                    out.println("Total faturado no mês " + m + " : " + this.model.getTotalFaturadoMesFilial(m, f));
                }
            }
            out.println("\n");
            out.println("Faturação conjunta das filiais: " + this.model.getTotalFaturado());
            for (int f = 1; f <= Constantes.N_FILIAIS; f++) {
                out.println("Filial " + f + ": ");
                for (int m = 1; m <= Constantes.N_MESES; m++) {
                    out.println("Total clientes que compraram no mês " + m + ": " + this.model.getTotalClientesCompraramMesFilial(m, f));
                }
                out.println("\n");
            }

        } catch (InvalidMonth | InvalidFilial e) {
            out.println(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query1() {
        List<String> codigos = new ArrayList<>();

        Crono.start();
        codigos.addAll(this.model.getCodigosProdutosNaoComprados());
        Crono.stop();

        this.view.displayList(codigos, "Códigos de Produto Não Comprados");

        out.println("Total produtos não usados: " + codigos.size());
        out.println(Crono.toShow());

        Input.lerEnter();
    }

    public void query2() {
        out.print("Mês: ");
        int mes = Input.lerInt();

        try {
            Crono.start();
            List<IParStringNum> result = new ArrayList<>();
            result.add(
                    new ParStringNum("Total: " + this.model.getTotalVendasMes(mes), this.model.getTotalClientesCompraramMes(mes))
            );

            for (int i = 1; i <= Constantes.N_FILIAIS; i++) {
                result.add(
                        new ParStringNum("Filial " + i + ": " + this.model.getTotalVendasMesFilial(mes, i),
                                this.model.getTotalClientesCompraramMesFilial(mes, i))
                );
            }
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("Vendas Realizadas (Mês " + mes + ")");
            header.add("Total de Clientes Distintos");

            Terminal.clear();
            GestVendasView.showLogo();
            this.view.displayQuickTable(result, "Total Global de Vendas", header);

            out.println(Crono.toShow());
        } catch (InvalidMonth | InvalidFilial e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query3() {
        out.print("Código de Cliente: ");
        String codigoCliente = Input.lerString();

        try {
            Crono.start();
            List<IParStringNum> result = new ArrayList<>();
            for (int mes = 1; mes <= Constantes.N_MESES; mes++) {
                result.add(
                        new ParStringNum(StringUtils.leftPad("(" + mes + ")", 4)
                                + "                    "
                                + this.model.getProdutosDiferentesMesCliente(codigoCliente, mes)
                                + " / " + this.model.totalComprasClienteMes(codigoCliente, mes)
                                , this.model.totalGastoClienteMes(codigoCliente, mes))
                );
            }
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("(Mês) Produtos Diferentes / Total de Compras");
            header.add("Total Gasto (€)");

            Terminal.clear();
            GestVendasView.showLogo();
            this.view.displayQuickTable(result, "Total de Compras (Cliente: " + codigoCliente + ") ", header);

            out.println(Crono.toShow());
        } catch (InvalidCliente | InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query4() {
        out.print("Código de Produto: ");
        String codigoProduto = Input.lerString();

        try {
            Crono.start();
            List<IParStringNum> result = new ArrayList<>();
            for (int mes = 1; mes <= Constantes.N_MESES; mes++) {
                result.add(
                        new ParStringNum(StringUtils.leftPad("(" + mes + ")", 4)
                                + "                    "
                                + this.model.getClientesDiferentesMesProduto(codigoProduto, mes)
                                + " / " + this.model.getComprasProdutoMes(codigoProduto, mes)
                                , this.model.getFaturacaoProdutoMes(codigoProduto, mes))
                );
            }
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("(Mês) Clientes Diferentes / Total de Vendas Efetuadas");
            header.add("Total Faturado (€)");

            Terminal.clear();
            GestVendasView.showLogo();
            this.view.displayQuickTable(result, "Total de Vendas (Produto: " + codigoProduto + ") ", header);

            out.println(Crono.toShow());
        } catch (InvalidProduto | InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query5() {
        out.print("Código de Cliente: ");
        String codigoCliente = Input.lerString();

        try {
            Crono.start();
            Set<IParStringNum> produtosCliente = this.model.getProdutosPorQuantidadeCliente(codigoCliente);
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("Código de Produto");
            header.add("Quantidade comprada");

            this.view.displayTable(produtosCliente, "Produtos mais comprados (Cliente " + codigoCliente + ") ", header);
            out.println("Tamanho: " + produtosCliente.size());
            out.println(Crono.toShow());
        } catch (InvalidCliente e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query6() {
        int x;
        do {
            out.print("Número: ");
            x = Input.lerInt();
        } while (x <= 0);

        try {
            Crono.start();
            List<IParStringNum> xProdutosMaisVendidos = this.model.getXProdutosMaisVendidosTotal(x);
            int clientesDiferentes;
            for (IParStringNum par : xProdutosMaisVendidos) {
                clientesDiferentes = this.model.getClientesDiferentesMesProduto((par.getString()), 0);
                par.setString(par.getString() + "                   (" + clientesDiferentes + ")");
            }
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("Produto (Clientes Diferentes)");
            header.add("Número de Unidades");

            this.view.displayTable(xProdutosMaisVendidos, "Top " + x + " Produtos Mais Vendidos Ano", header);
            out.println(Crono.toShow());
        } catch (InvalidProduto | InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query7() {
        Crono.start();
        List<List<IParStringNum>> maiores3f = this.model.get3MaioresCompradoresFiliais();
        Crono.stop();

        List<String> header = new ArrayList<>();
        header.add("Codigo do Cliente");
        header.add("Faturação (Euros)");

        Terminal.clear();
        GestVendasView.showLogo();
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            this.view.displayQuickTable(maiores3f.get(i), "Top 3 Maiores Compradores  (Filial " + (i + 1) + ")", header);
        }

        out.println(Crono.toShow());
        Input.lerEnter();
    }

    public void query8() {
        int x;
        do {
            out.print("Número: ");
            x = Input.lerInt();
        } while (x <= 0);

        try {
            Crono.start();
            List<IParStringNum> maioresX = this.model.getXClientesMaisProdutosDiferentes(x);
            Crono.stop();

            List<String> header = new ArrayList<>();
            header.add("Código Cliente");
            header.add("Quantidade de Produtos Diferentes");

            this.view.displayTable(maioresX, "Top " + x + " Clientes que Mais Compraram", header);
            out.println(Crono.toShow());
        } catch (InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query9() {
        out.print("Código de Produto: ");
        String codigoProduto = Input.lerString();

        int x;
        do {
            out.print("Número: ");
            x = Input.lerInt();
        } while (x <= 0);

        try {
            Crono.start();
            List<IParStringNum> maisCompraramProduto = this.model.getXClientesMaisCompraramProduto(codigoProduto, x);
            Crono.stop();

            for (int i = 0; i < maisCompraramProduto.size(); i++) {
                String key = maisCompraramProduto.get(i).getString();
                maisCompraramProduto.get(i).setString(
                        key + "          ("
                                + String.format("%.1f", this.model.getTotalGastoClienteProduto(key, codigoProduto)) + ") "
                );
            }

            List<String> header = new ArrayList<>();
            header.add("Código Cliente (Faturação €)");
            header.add("Quantidade");

            this.view.displayTable(maisCompraramProduto, "Top " + x + " Clientes (Produto: " + codigoProduto + ") ", header);
            out.println(Crono.toShow());
        } catch (InvalidProduto | InvalidCliente | InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }

    public void query10() {
        try {
            Crono.start();
            List<List<List<IParStringNum>>> lista = this.model.getFaturacaoProdutosMesFilial();
            Crono.stop();

            int mes = 1, filial = 1;

            List<String> header = new ArrayList<>();
            header.add("Código do Produto");
            header.add("Faturação em Euros");

            while (mes != 0 && filial != 0) {
                out.println("Mês (ou 0 para sair): ");
                mes = Input.lerOpcao(Constantes.N_MESES);

                out.println("Filial (ou 0 para sair): ");
                filial = Input.lerOpcao(Constantes.N_FILIAIS);

                if (mes != 0 && filial != 0)
                    this.view.displayTable(lista.get(mes - 1).get(filial - 1),
                            "Faturação Total (Filial: " + filial + " Mês: " + mes + ") ", header);
            }

            out.println(Crono.toShow());
        } catch (InvalidFilial | InvalidMonth e) {
            Terminal.error(e.getMessage());
        }

        Input.lerEnter();
    }
}
