package exceptions;

public class InvalidMonth extends Exception {

    public InvalidMonth(String message) {
        super(message);
    }
}
