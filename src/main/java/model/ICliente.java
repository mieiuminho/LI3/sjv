package model;

public interface ICliente {

    /**
     * Método que permite obter o código de um cliente.
     *
     * @return String com o código do cliente.
     */
    String getCodigo();

    /**
     * Método que permite definir o código de um cliente.
     *
     * @param codigo Código a definir.
     */
    void setCodigo(String codigo);

    /**
     * Método que permite obter uma cópia de um objeto do tipo `Cliente`
     *
     * @return Cópia do objeto do tipo `Cliente`
     */
    ICliente clone();
}
