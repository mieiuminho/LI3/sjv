package model;

import common.IParStringNum;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import model.faturacao.FaturacaoBase;
import model.faturacao.FaturacaoProduto;

import java.io.Serializable;
import java.util.*;

public class FaturacaoVector implements IFaturacao, Serializable {
    // Uma lista de 3 elementos (uma para cada filial) com:
    // uma lista com 13 elementos, uma para cada mês e no indice zero tem a faturacao anual
    private List<List<FaturacaoBase>> registos;

    /**
     * Construtor não parametrizado.
     */
    public FaturacaoVector() {
        this.registos = new Vector<>(Constantes.N_FILIAIS);

        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            List<FaturacaoBase> lst = new Vector<>(Constantes.N_MESES + 1);
            this.registos.add(lst);

            for (int j = 0; j <= Constantes.N_MESES; j++) {
                FaturacaoBase faturacaobase = new FaturacaoBase();
                this.registos.get(i).add(faturacaobase);
            }
        }
    }

    /**
     * Método que permite atualizar a estrutura `Faturacao`.
     *
     * @param filial        Número da filial.
     * @param mes           Número do mês.
     * @param codigoProduto Código do produto transacionado.
     * @param quantidade    Quantidade transacionada.
     * @param preco         Preço do produto transacionado.
     * @param tipo          Regime da transação (Promoção / Normal).
     */
    public void update(int filial, int mes, String codigoProduto, int quantidade, double preco, String tipo) {
        // atualizar o anual (indice zero dos meses)
        this.registos.get(filial - 1).get(0).update(codigoProduto, quantidade, preco, tipo);
        // atualizar o mês correspondente
        this.registos.get(filial - 1).get(mes).update(codigoProduto, quantidade, preco, tipo);
    }

    /**
     * Método que permite obter os códigos dos produtos não transacionados.
     *
     * @param codigos Conjunto dos códigos de produto registados.
     * @return Conjunto dos códigos dos produtos não transacionados.
     */
    public Set<String> getCodigosProdutosNaoComprados(Set<String> codigos) {
        Set<String> result = new TreeSet<>();

        for (String codigoProduto : codigos) {
            boolean usado = false;

            for (int i = 0; i < Constantes.N_FILIAIS; i++) {
                if (this.registos.get(i).get(0).exiteProduto(codigoProduto)) {
                    usado = true;
                }
            }

            if (!usado) {
                result.add(codigoProduto);
            }
        }

        return result;
    }

    /**
     * Método que permite obter o total de um vendas efetuadas num mês.
     *
     * @param mes Número do mês.
     * @return Número total de vendas efetuadas nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido
     */
    public int getTotalVendasMes(int mes) throws InvalidMonth {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        int ret = 0;

        for (List<FaturacaoBase> ls : this.registos) {
            ret += ls.get(mes).getTotalVendas();
        }

        return ret;
    }

    /**
     * Método que permite obter o total de vendas efetuada num mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número total de compras efetuadas nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public int getTotalVendasMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        if (filial < 1 || filial > Constantes.N_FILIAIS) throw new InvalidFilial("A filial " + filial + " é inválida!");
        return this.registos.get(filial - 1).get(mes).getTotalVendas();
    }

    /**
     * Método que permite obter o total faturado num mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Total faturado nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public double getTotalFaturadoMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        if (filial < 1 || filial > Constantes.N_FILIAIS) throw new InvalidFilial("A filial " + filial + " é inválida!");
        return this.registos.get(filial - 1).get(mes).getTotalFaturado();
    }

    /**
     * Método que permite obter o total faturado.
     *
     * @return Total faturado.
     */
    public double getTotalFaturado() {
        double ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            ret += this.registos.get(i).get(0).getTotalFaturado();
        }
        return ret;
    }

    /**
     * Método que permite obter o total de vendas num mês de um determinado produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param mes    Número do mês.
     * @return Total de vendas nesse mês desse produto.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public int getTotalVendasProdutoMes(String codigo, int mes) throws InvalidMonth {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        int totalVendas = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            totalVendas += this.registos.get(i).get(mes).getTotalVendasProduto(codigo);
        return totalVendas;
    }

    /**
     * Método que permite obter o total faturado com as vendas num mês de um determiando produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param mes    Número do mês.
     * @return Total faturado com as vendas desse produto nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public double getTotalFaturadoProdutoMes(String codigo, int mes) throws InvalidMonth {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        double receita = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            receita += this.registos.get(i).get(mes).getTotalFaturadoProduto(codigo);
        return receita;
    }

    /**
     * Método que permite obter a correspondência entre um produto e o seu número total de vendas.
     *
     * @return Correspondência entre um código de produto e o seu número total de vendas.
     */
    public Map<String, Integer> getProdutosVendidosTotal() {
        Map<String, Integer> produtosV = new HashMap<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            for (FaturacaoProduto fp : this.registos.get(i).get(0).getFaturacaoProdutos()) {
                String s = fp.getCodigoProduto();
                if (produtosV.containsKey(s))
                    produtosV.put(s, produtosV.get(s) + fp.getTotalUnidades());
                else produtosV.put(s, fp.getTotalUnidades());
            }
        return produtosV;
    }

    /**
     * Método que permite o obter o total faturado numa filial por mês.
     *
     * @param filial Número da filial.
     * @param mes    Número do mês.
     * @return Correspondência entre filial, mês e o total faturado.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public List<IParStringNum> getTotaisFaturadoFilialMes(int filial, int mes) throws InvalidMonth, InvalidFilial {
        if (mes < 1 || mes > Constantes.N_MESES) throw new InvalidMonth("O mês " + mes + " é inválido!");
        if (filial < 1 || filial > Constantes.N_FILIAIS) throw new InvalidFilial("A filial " + filial + " é inválida!");
        Map<String, Double> temp = new HashMap<>();
        List<IParStringNum> ret = new ArrayList<>();
        for (FaturacaoProduto fp : this.registos.get(filial - 1).get(mes).getFaturacaoProdutos())
            temp.put(fp.getCodigoProduto(), fp.getTotalFaturado());
        for (Map.Entry<String, Double> par : temp.entrySet())
            ret.add(new ParProdutoFaturacao(par.getKey(), par.getValue()));
        return ret;
    }

}
