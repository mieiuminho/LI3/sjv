package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class CatClientes implements ICatClientes, Serializable {

    private Set<ICliente> clientes;
    private int usados;

    /**
     * Construtor não parametrizado
     */
    public CatClientes() {
        this.clientes = new HashSet<>();
        this.usados = 0;
    }

    /**
     * Método que permite obter o número de clientes
     *
     * @return Número de clientes
     */
    public int getTotal() {
        return this.clientes.size();
    }

    /**
     * Método que permite obter o número de clientes envolvidos em transações
     *
     * @return Número de clientes envolvidos em transações.
     */
    public int getUsados() {
        return this.usados;
    }

    /**
     * Método que permite obter o número de clientes não envolvidos em transações.
     *
     * @return Número de clientes não envolvidos em transações.
     */
    public int getNaoUsados() {
        return this.getTotal() - this.getUsados();
    }

    /**
     * Método que permite obter os clientes registados.
     *
     * @return `Set` que contém os clientes registados.
     */
    public Set<ICliente> getClientes() {
        Set<ICliente> lst = new TreeSet<>();

        for (ICliente cli : this.clientes) {
            lst.add(cli.clone());
        }

        return lst;
    }

    /**
     * Método que permite inserir um cliente na estrutura de clientes.
     *
     * @param cliente Cliente a inserir.
     */
    public void insereCliente(ICliente cliente) {
        this.clientes.add(cliente.clone());
    }

    /**
     * Método que permite determinar se um cliente se encontra registado ou não.
     *
     * @param cliente Cliente a averiguar.
     * @return `true` caso o cliente argumento esteja registado ou `false` caso contrário.
     */
    public boolean existeCliente(ICliente cliente) {
        return this.clientes.contains(cliente);
    }

    /**
     * Método que permite determinar se um cliente com o código argumento se encontra registado ou não.
     *
     * @param codigoCliente Código de cliente a averiguar.
     * @return `true` caso o cliente com aquele código esteja registado ou `false` caso contrário.
     */
    public boolean existeCliente(String codigoCliente) {
        return this.clientes.contains(new Cliente(codigoCliente));
    }
}
