package model;

import common.IParStringNum;
import exceptions.InvalidMonth;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IFilial {

    /**
     * Métoodo que permite atualizar a estrutura `Filial`.
     *
     * @param codigoCliente Código do cliente envolvido na transação.
     * @param mes           Número do mês.
     * @param codigoProduto Código do produto envolvido na transação.
     * @param quantidade    Quantidade transacionada.
     * @param preco         Preço do produto.
     * @param tipo          Regime da transação (Promoção / Normal).
     */
    void update(String codigoCliente, int mes, String codigoProduto, int quantidade, double preco, String tipo);

    /**
     * Método que permite obter o número de clientes que realizaram compras num dado mês.
     *
     * @param mes Número do mês.
     * @return Número de clientes que realizou compras no mês argumento.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    int getTotalClientesCompraramMes(int mes) throws InvalidMonth;

    /**
     * Método que permite determinar quais os produtos comprados por um dado cliente num dado mês.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Conjunto dos códigos dos produtos comprados por esse clientes nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    Set<String> getCodigosProdutosCompradosPorCliente(String codigo, int mes) throws InvalidMonth;

    /**
     * Método que permite obter o total gasto por um cliente num dado mês.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Total gasto pelo cliente nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    double getTotalGastoClienteMes(String codigo, int mes) throws InvalidMonth;

    /**
     * Método que permite obter o número de compras realizadas num dado mês por um dado cliente.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Número de compras realizadas por esse cliente nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    int getNumeroComprasClienteMes(String codigo, int mes) throws InvalidMonth;

    /**
     * Método que permite obter os códigos dos cliente que compraram um dado produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Conjunto dos códigos dos clientes que compraram aquele produto nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    Set<String> getCodigosClientesCompraramProduto(String codigoProduto, int mes) throws InvalidMonth;

    /**
     * Método que permite determinar quantas vezes um cliente comprou cada produto.
     *
     * @param codigoCliente Cliente a averiguar.
     * @return Associação entre os códigos de produto e o número de vezes que o cliente adquiriu cada um.
     */
    Map<String, Integer> getQuantidadesProdutosCliente(String codigoCliente);

    /**
     * Método que permite obter uma lista de associações entre os 3 maiores compradores e total de dinheiro que gastaram.
     *
     * @return Lista (com 3 elementos) de associações entre o código de cliente dos maiores compradores e o total de
     * dinheiro dispendido.
     */
    List<IParStringNum> get3MaioresCompradores();

    /**
     * Método que permite obter os códigos dos produtos que cada cliente comprou num dado mês.
     *
     * @param mes Número do mễs.
     * @return Associação entre cada cliente e os códigos dos produtos que adquiriu naquele mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    Map<String, Set<String>> getCodigosProdutosCompradosClientesMes(int mes) throws InvalidMonth;

    /**
     * Método que permite obter o número de vezes que um determinado cliente comprou um determinado produto.
     *
     * @param codigoProduto   Código do produto a averiguar.
     * @param codigosClientes Código do cliente a averiguar.
     * @return Número de vezes que o cliente adquiriu aquele produto.
     */
    Map<String, Integer> getNumeroComprasProdutoClientes(String codigoProduto, Set<String> codigosClientes);

    /**
     * Método que permite determinar o total faturado com um cliente e com um determinado produto.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param codigoCliente Código do cliente a averiguar.
     * @return Total faturado com o cliente a comprar o produto.
     */
    double getTotalFaturadoProdutoCliente(String codigoProduto, String codigoCliente);

    /**
     * Método que permite os códigos dos clientes que realizaram compras num dado mês.
     *
     * @param mes Número do mês.
     * @return Conjunto dos códigos de cliente que realizaram compras naquele mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    Set<String> getCodigosClientesMes(int mes) throws InvalidMonth;

}
