package performance.models;

import common.IParStringNum;
import exceptions.InvalidCliente;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;
import model.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class ModelHashHashArray implements IGestVendasModel, Serializable {

    private ICatProdutos catProdutos;
    private ICatClientes catClientes;
    private IFaturacao facturacao;
    private List<IFilial> filiais;
    private int nrVendasErradas;
    private String ultimoFicheiroLido;

    private static final String DATABASE_PATH = "data/GestVendas.dat";

    public void save() throws IOException {
        Parse.saveObject(this, DATABASE_PATH);
    }

    public model.GestVendasModel load() throws ClassNotFoundException, IOException {
        return (model.GestVendasModel) Parse.loadObject(DATABASE_PATH);
    }

    public int getNrVendasErradas() {
        return nrVendasErradas;
    }

    public String getUltimoFicheiroLido() {
        return this.ultimoFicheiroLido;
    }

    public void createData(String ficheiroClientes, String ficheiroProdutos, String ficheiroVendas) {
        this.catClientes = Parse.loadCatClientes(ficheiroClientes); // HashSet
        this.catProdutos = Parse.loadCatProdutos(ficheiroProdutos); // HashSet
        this.facturacao = new Faturacao(); // ArrayList<HashMap>
        this.filiais = new ArrayList<>(); // ArrayList
        this.nrVendasErradas = 0;

        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            IFilial fl = new Filial(); // ArrayList<HashMap>
            filiais.add(fl);
        }

        BufferedReader inFile;
        String linha;
        IVenda venda;

        try {
            inFile = new BufferedReader(new FileReader(ficheiroVendas));
            while ((linha = inFile.readLine()) != null) {
                venda = Parse.linhaToVenda(linha);
                if (venda != null && venda.isValid() && this.catClientes.existeCliente(venda.getCliente()) && this.catProdutos.existeProduto(venda.getProduto())) {
                    this.filiais.get(venda.getFilial() - 1)
                            .update(venda.getCodigoCliente(), venda.getMes(), venda.getCodigoProduto(), venda.getQuantidade(), venda.getPreco(), venda.getTipo());
                    this.facturacao.update(venda.getFilial(), venda.getMes(), venda.getCodigoProduto(), venda.getQuantidade(), venda.getPreco(), venda.getTipo());
                } else this.nrVendasErradas++;
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }
    }

    public int getNumFiliais() {
        return this.filiais.size();
    }

    public Set<String> getCodigosProdutosNaoComprados() {
        Set<String> codigosProdutos = this.catProdutos.getCodigosProdutos();

        Set<String> codigosProdutosNaoComprados = this.facturacao.getCodigosProdutosNaoComprados(codigosProdutos);

        return codigosProdutosNaoComprados;
    }

    public int getTotalClientes() {
        return this.catClientes.getTotal();
    }

    public int getTotalProdutos() {
        return this.catProdutos.getTotal();
    }

    public int getTotalProdutosNaoComprados() {
        return this.getCodigosProdutosNaoComprados().size();
    }

    public int getTotalClientesCompraram() {
        Set<ICliente> clientes = this.catClientes.getClientes();
        boolean usado;
        int ret = 0;
        for (ICliente cliente : clientes) {
            usado = false;
            for (int i = 0; i < Constantes.N_FILIAIS && !usado; i++)
                try {
                    if (this.filiais.get(i).getNumeroComprasClienteMes(cliente.getCodigo(), 0) > 0)
                        usado = true;
                } catch (InvalidMonth e) {
                    return 0;
                }
            if (usado) ret++;
        }
        return ret;
    }

    public int getTotalVendasMes(int mes) throws InvalidMonth {
        return this.facturacao.getTotalVendasMes(mes);
    }

    public int getTotalVendasMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        return this.facturacao.getTotalVendasMesFilial(mes, filial);
    }

    public double getTotalFaturadoMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        return this.facturacao.getTotalFaturadoMesFilial(mes, filial);
    }

    public double getTotalFaturado() {
        return this.facturacao.getTotalFaturado();
    }

    public int getTotalClientesCompraramMes(int mes) throws InvalidMonth {
        Set<String> totalclientes = new TreeSet<>();
        for (IFilial filial : this.filiais)
            totalclientes.addAll(filial.getCodigosClientesMes(mes));
        return totalclientes.size();
    }

    public int getTotalClientesCompraramMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        if (filial < 1 || filial > Constantes.N_FILIAIS) throw new InvalidFilial("A filial " + filial + " é inválida!");

        return this.filiais.get(filial - 1).getTotalClientesCompraramMes(mes);
    }

    public int getProdutosDiferentesMesCliente(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        HashSet<String> todasFiliais = new HashSet<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            todasFiliais.addAll(this.filiais.get(i).getCodigosProdutosCompradosPorCliente(codigoCliente, mes));
        return (todasFiliais.size());
    }

    public double totalGastoClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        double ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getTotalGastoClienteMes(codigoCliente, mes);
        return ret;
    }

    public int totalComprasClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        int ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getNumeroComprasClienteMes(codigoCliente, mes);
        return ret;
    }

    public int getClientesDiferentesMesProduto(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        HashSet<String> todasFiliais = new HashSet<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            todasFiliais.addAll(this.filiais.get(i).getCodigosClientesCompraramProduto(codigoProduto, mes));
        return (todasFiliais.size());
    }

    public int getComprasProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        return this.facturacao.getTotalVendasProdutoMes(codigoProduto, mes);
    }

    public double getFaturacaoProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        return this.facturacao.getTotalFaturadoProdutoMes(codigoProduto, mes);
    }

    public Set<IParStringNum> getProdutosPorQuantidadeCliente(String codigoCliente) throws InvalidCliente {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        Map<String, Integer> totalCliente = new HashMap<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            for (Map.Entry<String, Integer> par : this.filiais.get(i).getQuantidadesProdutosCliente(codigoCliente).entrySet()) {
                String s = par.getKey();
                if (totalCliente.containsKey(s))
                    totalCliente.put(s, totalCliente.get(s) + par.getValue());
                else totalCliente.put(s, par.getValue());
            }
        }
        Set<IParStringNum> ret = new TreeSet<>(new CompareParValueString());
        for (Map.Entry<String, Integer> par : totalCliente.entrySet())
            ret.add(new ParProdutoQuantidade(par.getKey(), par.getValue()));
        return ret;
    }

    private List<IParStringNum> getFirstXClienteQuantidade(Map<String, Integer> arg, int x) {
        Set<IParStringNum> temp = new TreeSet<>(new CompareParValueString());
        List<IParStringNum> ret = new ArrayList<>();
        for (Map.Entry<String, Integer> par : arg.entrySet())
            temp.add(new ParClienteQuantidade(par.getKey(), par.getValue()));
        int i = 0;
        Iterator<IParStringNum> it = temp.iterator();
        while (it.hasNext() && i < x) {
            ret.add(it.next());
            i++;
        }
        return ret;
    }

    public List<IParStringNum> getXProdutosMaisVendidosTotal(int x) {
        Map<String, Integer> produtosVendidos = this.facturacao.getProdutosVendidosTotal();
        Set<IParStringNum> pares = new TreeSet<>(new CompareParValueString());
        for (Map.Entry<String, Integer> par : produtosVendidos.entrySet())
            pares.add(new ParProdutoQuantidade(par.getKey(), par.getValue()));
        return pares.stream().limit(x).collect(Collectors.toList());
    }

    public List<List<IParStringNum>> get3MaioresCompradoresFiliais() {
        List<List<IParStringNum>> ret = new ArrayList<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret.add(this.filiais.get(i).get3MaioresCompradores());
        return ret;
    }

    public List<IParStringNum> getXClientesMaisProdutosDiferentes(int x) throws InvalidMonth {
        Map<String, Set<String>> totalClientes = new HashMap<>();
        Map<String, Set<String>> clientesFilial;
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            clientesFilial = this.filiais.get(i).getCodigosProdutosCompradosClientesMes(0);
            for (Map.Entry<String, Set<String>> par : clientesFilial.entrySet()) {
                String s = par.getKey();
                if (totalClientes.containsKey(s)) {
                    Set<String> temp = totalClientes.get(s);
                    temp.addAll(par.getValue());
                    totalClientes.put(s, temp);
                } else
                    totalClientes.put(s, par.getValue());
            }
        }
        Map<String, Integer> temp = new HashMap<>();
        for (Map.Entry<String, Set<String>> par : totalClientes.entrySet())
            temp.put(par.getKey(), par.getValue().size());
        return this.getFirstXClienteQuantidade(temp, x);
    }

    public List<IParStringNum> getXClientesMaisCompraramProduto(String codigo, int x) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigo)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        Map<String, Integer> totalCliente = new HashMap<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            for (Map.Entry<String, Integer> par : this.filiais.get(i).getNumeroComprasProdutoClientes(codigo, this.filiais.get(i).getCodigosClientesCompraramProduto(codigo, 0)).entrySet()) {
                String s = par.getKey();
                if (totalCliente.containsKey(s))
                    totalCliente.put(s, totalCliente.get(s) + par.getValue());
                else totalCliente.put(s, par.getValue());
            }
        }
        return this.getFirstXClienteQuantidade(totalCliente, x);
    }

    public double getTotalGastoClienteProduto(String codigoCliente, String codigoProduto) throws InvalidProduto, InvalidCliente {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        double ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getTotalFaturadoProdutoCliente(codigoProduto, codigoCliente);
        return ret;
    }

    public List<List<List<IParStringNum>>> getFaturacaoProdutosMesFilial() throws InvalidMonth, InvalidFilial {
        List<List<List<IParStringNum>>> ret = new ArrayList<>();
        for (int m = 1; m <= Constantes.N_MESES; m++) {
            List<List<IParStringNum>> temp = new ArrayList<>();
            for (int f = 1; f <= Constantes.N_FILIAIS; f++)
                temp.add(this.facturacao.getTotaisFaturadoFilialMes(f, m));
            ret.add(temp);
        }
        return ret;
    }


}
