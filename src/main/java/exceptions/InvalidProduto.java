package exceptions;

public class InvalidProduto extends Exception {

    public InvalidProduto(String error) {
        super(error);
    }
}
