package model;

import java.io.Serializable;

public class Venda implements IVenda, Serializable {

    private IProduto produto;
    private double preco;
    private int quantidade;
    private String tipo;
    private ICliente cliente;
    private int mes;
    private int filial;

    /**
     * Construtor parametrizado.
     *
     * @param codigoProduto Código do produto.
     * @param preco         Preço do produto.
     * @param quantidade    Quantidade transacionada.
     * @param tipo          Regime da compra (Promoção / Normal).
     * @param codigoCliente Código do cliente.
     * @param mes           Número do mês.
     * @param filial        Número da filial.
     */
    public Venda(String codigoProduto, double preco, int quantidade, String tipo, String codigoCliente, int mes, int filial) {
        this.produto = new Produto(codigoProduto);
        this.preco = preco;
        this.quantidade = quantidade;
        this.tipo = tipo;
        this.cliente = new Cliente(codigoCliente);
        this.mes = mes;
        this.filial = filial;
    }

    /**
     * Construtor parametrizado.
     *
     * @param venda Objeto de uma classe que satisfaz a interface `IVenda`.
     */
    public Venda(IVenda venda) {
        this.produto = venda.getProduto();
        this.preco = venda.getPreco();
        this.quantidade = venda.getQuantidade();
        this.tipo = venda.getTipo();
        this.cliente = venda.getCliente();
        this.mes = venda.getMes();
        this.filial = venda.getFilial();
    }

    /**
     * Método que permite obter uma cópia do produto.
     *
     * @return Cópia do produto.
     */
    public IProduto getProduto() {
        return this.produto.clone();
    }

    /**
     * Método que permite obter o código do produto.
     *
     * @return String com o código do produto.
     */
    public String getCodigoProduto() {
        return this.produto.getCodigo();
    }

    /**
     * Método que permite obter o preço do produto.
     *
     * @return Preço do produto.
     */
    public double getPreco() {
        return this.preco;
    }

    /**
     * Método que permite obter a quantidade transacionada.
     *
     * @return Quantidade transacionada.
     */
    public int getQuantidade() {
        return this.quantidade;
    }

    /**
     * Método que permite obter o regime em que aconteceu a transação (Promoção / Normal).
     *
     * @return Regime em que ocorreu a transação.
     */
    public String getTipo() {
        return this.tipo;
    }

    /**
     * Método que permite obter o cliente autor da transação.
     *
     * @return Cliente autor da transação.
     */
    public ICliente getCliente() {
        return this.cliente.clone();
    }

    /**
     * Método que permite obter o código do cliente autor da transação.
     *
     * @return Código do cliente autor da transação.
     */
    public String getCodigoCliente() {
        return this.cliente.getCodigo();
    }

    /**
     * Método que permite obter o mês em que ocorreu a transação.
     *
     * @return Mês em que ocorreu a transação.
     */
    public int getMes() {
        return this.mes;
    }

    /**
     * Método que permite obter a filial em que ocorreu a transação.
     *
     * @return Filial em que ocorreu a transação.
     */
    public int getFilial() {
        return this.filial;
    }

    /**
     * Método que permite obter o valor transacionado.
     *
     * @return Valor transacionado.
     */
    public double getValor() {
        return this.quantidade * this.preco;
    }

    /**
     * Método que determina se uma venda é válida.
     *
     * @return `true` caso afirmativo ou `false` caso contrário.
     */
    public boolean isValid() {
        return preco >= 0.0 && preco <= 999.99 && quantidade >= 1 && quantidade <= 200 &&
                (tipo.equals("N") || tipo.equals("P")) && mes >= 1 && mes <= 12 &&
                filial >= 1 && filial <= Constantes.N_FILIAIS;
    }

    /**
     * Método que permite obter uma cópia do objeto que satisfaça a interface `IVenda`.
     *
     * @return Cópia do objeto.
     */
    @Override
    public IVenda clone() {
        return new Venda(this);
    }

    /**
     * Método que permite obter uma representação textual de um objeto do tipo `Venda`.
     *
     * @return Representação textual do objeto.
     */
    @Override
    public String toString() {
        return "Venda {" +
                " codigoProduto='" + this.getCodigoProduto() + '\'' +
                ", preco=" + this.preco +
                ", quantidade=" + this.quantidade +
                ", tipo='" + this.tipo + '\'' +
                ", codigoCliente='" + this.getCodigoCliente() + '\'' +
                ", mes=" + this.mes +
                ", filial=" + this.filial +
                '}';
    }
}
