package model.filial;

import model.IProduto;
import model.Produto;

import java.io.Serializable;

public class InfoProduto implements Serializable {
    private IProduto produto;
    private int quantidadeN;
    private int quantidadeP;
    private double faturacaoN;
    private double faturacaoP;

    public InfoProduto(String codigoProduto) {
        this.produto = new Produto(codigoProduto);
        this.quantidadeN = 0;
        this.quantidadeP = 0;
        this.faturacaoN = 0;
        this.faturacaoP = 0;
    }

    public InfoProduto(IProduto produto, int quantidadeN, int quantidadeP, double faturacaoN, double faturacaoP) {
        this.produto = produto;
        this.quantidadeN = quantidadeN;
        this.quantidadeP = quantidadeP;
        this.faturacaoN = faturacaoN;
        this.faturacaoP = faturacaoP;
    }

    public InfoProduto(String codigoProduto, String tipo, int quantidade, double faturacao) {
        this.produto = new Produto(codigoProduto);
        if (tipo.equals("P")) {
            this.quantidadeP = quantidade;
            this.faturacaoP = faturacao;
        } else {
            this.quantidadeN = quantidade;
            this.faturacaoN = faturacao;
        }
    }

    public InfoProduto(InfoProduto infprd) {
        this.produto = infprd.getProduto();
        this.quantidadeN = infprd.getQuantidadeN();
        this.quantidadeP = infprd.getQuantidadeP();
        this.faturacaoN = infprd.getFaturacaoN();
        this.faturacaoP = infprd.getFaturacaoP();
    }

    public void update(String regime, int quantidade, double faturacao) {
        if (regime.equals("P")) {
            this.quantidadeP += quantidade;
            this.faturacaoP += faturacao;
        } else {
            this.quantidadeN += quantidade;
            this.faturacaoN += faturacao;
        }
    }

    public IProduto getProduto() {
        return this.produto.clone();
    }

    public String getCodigoProduto() {
        return this.produto.getCodigo();
    }

    public int getQuantidadeN() {
        return this.quantidadeN;
    }

    public int getQuantidadeP() {
        return this.quantidadeP;
    }

    public int getQuantidadeTotal() {
        return this.quantidadeN + this.quantidadeP;
    }

    public double getFaturacaoN() {
        return this.faturacaoN;
    }

    public double getFaturacaoP() {
        return this.faturacaoP;
    }

    public double getFaturacaoTotal() {
        return this.faturacaoN + this.faturacaoP;
    }

    @Override
    public InfoProduto clone() {
        return new InfoProduto(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InfoProduto that = (InfoProduto) o;

        return this.quantidadeN == that.quantidadeN &&
                this.quantidadeP == that.quantidadeP &&
                Double.compare(that.faturacaoN, this.faturacaoN) == 0 &&
                Double.compare(that.faturacaoP, this.faturacaoP) == 0 &&
                this.produto.equals(that.produto);
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int hash = 7;
        hash = prime * hash + (this.produto == null ? 0 : this.produto.hashCode());
        hash = (int) (prime * hash + this.quantidadeN + this.quantidadeP + this.faturacaoN + this.faturacaoP);
        return hash;
    }
}
