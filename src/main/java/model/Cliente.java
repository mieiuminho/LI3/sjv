package model;

import java.io.Serializable;

public class Cliente implements ICliente, Comparable<Cliente>, Serializable {

    private String codigo;

    /**
     * Construtor parametrizado.
     *
     * @param codigo Código de cliente.
     */
    public Cliente(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Construtor parametrizado.
     *
     * @param cliente Estrutura cliente.
     */
    public Cliente(Cliente cliente) {
        this.codigo = cliente.getCodigo();
    }

    /**
     * Método que permite obter o código de um cliente.
     *
     * @return String com o código do cliente.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Método que permite definir o código de um cliente.
     *
     * @param codigo Código a definir.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Método que permite obter uma cópia de um objeto do tipo `Cliente`
     *
     * @return Cópia do objeto do tipo `Cliente`
     */
    @Override
    public Cliente clone() {
        return new Cliente(this);
    }

    /**
     * Método que permite determinar se dois objetos do tipo `Cliente` são equivalentes.
     *
     * @param o `Cliente` a comparar.
     * @return `true` se os objetos forem equivalentes ou `false` caso contrário.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Cliente cliente = (Cliente) o;

        return this.codigo != null ? this.codigo.equals(cliente.getCodigo()) : cliente.getCodigo() == null;
    }

    /**
     * Método que calcula o `hashCode`.
     *
     * @return `hashCode`.
     */
    @Override
    public int hashCode() {
        return this.codigo != null ? this.codigo.hashCode() : 0;
    }

    /**
     * Método que permite obter uma representação textual de um objeto do tipo `Cliente`.
     *
     * @return String com a representação textual do objeto do tipo `Cliente`.
     */
    @Override
    public String toString() {
        return this.codigo;
    }

    /**
     * Método que permite comparar dois objetos do tipo `Cliente` (ordem natural).
     *
     * @param cliente `Cliente` a comparar.
     * @return Um inteiro menor que zero caso o objeto a que alicamos o método venha antes do objeto argumento na ordem
     * natural, um inteiro maior que zero caso o objeto a que aplicamos o método venha depois do objeto argumento na
     * ordem natural ou zero caso ocupem a mesma posição.
     */
    @Override
    public int compareTo(Cliente cliente) {
        return this.codigo.compareTo(cliente.getCodigo());
    }
}
