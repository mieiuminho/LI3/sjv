package performance;

import common.Crono;
import model.*;
import net.steppschuh.markdowngenerator.table.Table;
import org.junit.*;
import view.Color;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static java.lang.System.out;

public class TestLeituraVendas {
    private static final String RESOURCES_PATH = "target/classes/";
    private static final String CLIENTS_FILE = RESOURCES_PATH + "Clientes.txt";
    private static final String PRODUCTS_FILE = RESOURCES_PATH + "Produtos.txt";

    private static ICatClientes catClientes;
    private static ICatProdutos catProdutos;

    private static Map<String, List<Double>> reading;
    private static Map<String, List<Double>> parsing;
    private static Map<String, List<Double>> validating;

    private String title;
    private String file;
    private List<Double> read;
    private List<Double> parse;
    private List<Double> validate;

    @BeforeClass
    public static void setUpBeforeAll() {
        TestLeituraVendas.reading = new HashMap<>();
        TestLeituraVendas.parsing = new HashMap<>();
        TestLeituraVendas.validating = new HashMap<>();
        TestLeituraVendas.catClientes = Parse.loadCatClientes(TestLeituraVendas.CLIENTS_FILE);
        TestLeituraVendas.catProdutos = Parse.loadCatProdutos(TestLeituraVendas.PRODUCTS_FILE);
    }

    @Before
    public void setUp() {
        this.title = "";
        this.file = "";
        this.read = new ArrayList<>();
        this.parse = new ArrayList<>();
        this.validate = new ArrayList<>();
    }

    @After
    public void tearDown() {
        TestLeituraVendas.reading.put(this.title, new ArrayList<>(this.read));
        TestLeituraVendas.parsing.put(this.title, new ArrayList<>(this.parse));
        TestLeituraVendas.validating.put(this.title, new ArrayList<>(this.validate));
    }

    @AfterClass
    public static void tearDownAfterAll() {
        Table.Builder table_reading = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT, Table.ALIGN_RIGHT)
                .addRow("Ficheiro de Vendas (Leitura)", "lerLinhasWithBuff", "readAllLines");

        for (Map.Entry<String, List<Double>> result : TestLeituraVendas.reading.entrySet()) {
            table_reading.addRow(result.getKey()
                    , String.format("%.3f", result.getValue().get(0))
                    , String.format("%.3f", result.getValue().get(1))
            );
        }

        Table.Builder table_parsing = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT, Table.ALIGN_RIGHT)
                .addRow("Ficheiro de Vendas (Leitura + Parsing)", "lerLinhasWithBuff", "readAllLines");

        for (Map.Entry<String, List<Double>> result : TestLeituraVendas.parsing.entrySet()) {
            table_parsing.addRow(result.getKey()
                    , String.format("%.3f", result.getValue().get(0))
                    , String.format("%.3f", result.getValue().get(1))
            );
        }

        Table.Builder table_validating = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT, Table.ALIGN_RIGHT)
                .addRow("Ficheiro de Vendas (Leitura + Parsing)", "lerLinhasWithBuff", "readAllLines");

        for (Map.Entry<String, List<Double>> result : TestLeituraVendas.validating.entrySet()) {
            table_validating.addRow(result.getKey()
                    , String.format("%.3f", result.getValue().get(0))
                    , String.format("%.3f", result.getValue().get(1))
            );
        }


        try {
            Parse.writeFile("tables/reading_5M.md", Arrays.asList(new String[]{table_reading.build().toString()}));
            Parse.writeFile("tables/parsing_5M.md", Arrays.asList(new String[]{table_parsing.build().toString()}));
            Parse.writeFile("tables/validating_5M.md", Arrays.asList(new String[]{table_validating.build().toString()}));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> lerLinhasWithBuff(String fichtxt) {
        List<String> linhas = new ArrayList<>();
        BufferedReader inFile;
        String linha;

        try {
            inFile = new BufferedReader(new FileReader(fichtxt));
            while ((linha = inFile.readLine()) != null) linhas.add(linha);
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return linhas;
    }

    public static List<String> lerAllLines(String fichtxt) {
        List<String> linhas = new ArrayList<>();
        try {
            linhas = Files.readAllLines(Paths.get(fichtxt));
        } catch (IOException exc) {
            out.println(exc);
        }
        return linhas;
    }

    public static List<IVenda> parseVendas(List<String> lines) {
        List<IVenda> vendas = new ArrayList<>();
        IVenda venda;

        for (String line : lines) {
            venda = Parse.linhaToVenda(line);
            if (venda != null) {
                vendas.add(venda);
            }
        }

        return vendas;
    }

    public static List<IVenda> createVendas(List<String> lines, ICatClientes catcli, ICatProdutos catprod) {
        List<IVenda> vendas = new ArrayList<>();
        IVenda venda;

        for (String line : lines) {
            venda = Parse.linhaToVenda(line);
            if (venda != null && venda.isValid() && catcli.existeCliente(venda.getCliente()) && catprod.existeProduto(venda.getProduto())) {
                vendas.add(venda);
            }
        }

        return vendas;
    }

    @Ignore
    public void createData() {
        ICatClientes catcli = Parse.loadCatClientes(Constantes.FILE_CLIENTES);
        Assert.assertEquals(16384, catcli.getTotal());

        ICatProdutos catprod = Parse.loadCatProdutos(Constantes.FILE_PRODUTOS);
        Assert.assertEquals(171008, catprod.getTotal());

        List<IVenda> vendas = Parse.loadVendas(Constantes.FILE_VENDAS, catcli, catprod);
        Assert.assertEquals(891108, vendas.size());
    }


    @Test
    public void testReadingParsingValidating() {
        this.title = "1 Milhões de Vendas";
        this.file = TestLeituraVendas.RESOURCES_PATH + "Vendas_1M.txt";

        this.testReading();
        this.testParsing();
        this.testValidating();
    }

    public void testReading() {
        List<String> linhas;
        List<IVenda> vendas;

        out.println(Color.ANSI_BLUE + this.title + Color.ANSI_RESET);

        Crono.start();
        linhas = lerLinhasWithBuff(this.file);
        Crono.stop();
        this.read.add(Crono.time());
        out.println("Reading with buffer: " + Crono.time() + " segundos");

        Crono.start();
        linhas = lerAllLines(this.file);
        Crono.stop();
        this.read.add(Crono.time());
        out.println("Reading will all lines: " + Crono.time() + " segundos");

        out.print("\n");
    }

    public void testParsing() {
        List<String> linhas;
        List<IVenda> vendas;

        Crono.start();
        linhas = lerLinhasWithBuff(this.file);
        vendas = parseVendas(linhas);
        Crono.stop();
        this.parse.add(Crono.time());
        out.println("Parsing with buffer: " + Crono.time() + " segundos");

        Crono.start();
        linhas = lerAllLines(this.file);
        vendas = parseVendas(linhas);
        Crono.stop();
        this.parse.add(Crono.time());
        out.println("Parsing will all lines: " + Crono.time() + " segundos");

        out.print("\n");
    }

    public void testValidating() {
        List<String> linhas;
        List<IVenda> vendas;

        Crono.start();
        linhas = lerLinhasWithBuff(this.file);
        vendas = createVendas(linhas, TestLeituraVendas.catClientes, TestLeituraVendas.catProdutos);
        Crono.stop();
        this.validate.add(Crono.time());
        out.println("Validating with buffer: " + Crono.time() + " segundos");

        Crono.start();
        linhas = lerAllLines(this.file);
        vendas = createVendas(linhas, TestLeituraVendas.catClientes, TestLeituraVendas.catProdutos);
        Crono.stop();
        this.validate.add(Crono.time());
        out.println("Validating will all lines: " + Crono.time() + " segundos");

        out.print("\n");
    }
}
