package view;

import common.IParStringNum;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.lang.System.out;

public class GestVendasView implements IGestVendasView {
    public static final String ASCII_LOGO_PATH = "img/logo.ascii";

    public static final String[] opcoesMenuPrincipal = {
            "Ler dados",
            "Gravar dados",
            "Carregar dados",
            "Consultas Estatisticas",
            "Consultas Interativas",
            "Sair",
    };

    public static final String[] opcoesMenuEstatisticas = {
            "Estatísticas Ficheiro",
            "Estatísticas Globais",
            "Sair",
    };

    public static final String[] opcoesMenuQueries = {
            "Query 1",
            "Query 2",
            "Query 3",
            "Query 4",
            "Query 5",
            "Query 6",
            "Query 7",
            "Query 8",
            "Query 9",
            "Query 10",
            "Sair",
    };

    public int menu(String title, String[] options) {
        Terminal.clear();
        GestVendasView.showLogo();
        new Menu(title, options).show();
        out.println("\n");
        return Input.lerOpcao(options.length - 1);
    }

    public static List<String> readFile(String file) {
        List<String> lines = new ArrayList<>();
        BufferedReader inFile;
        String line;

        try {
            inFile = new BufferedReader(new FileReader(file));
            while ((line = inFile.readLine()) != null) lines.add(line);
        } catch (IOException e) {
            Terminal.error(e.getMessage());
        }

        return lines;
    }

    public static void showLogo() {
        List<String> lines = readFile(ASCII_LOGO_PATH);

        out.print(Color.ANSI_YELLOW);
        for (String line : lines) {
            System.out.println(StringUtils.center(line, Terminal.WINDOW_WIDTH));
        }
        out.print(Color.ANSI_RESET);
    }

    public void displayTable(Collection<IParStringNum> lines, String title, List<String> fields) {
        new Navegador(lines, title, fields).show();
    }

    public void displayQuickTable(Collection<IParStringNum> lines, String title, List<String> fields) {
        new Navegador(lines, title, fields).displayQuickTable();
    }

    public void displayList(Collection<String> lines, String title) {
        new Navegador(lines, title).show();
    }
}
