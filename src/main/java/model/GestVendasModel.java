package model;

import common.IParStringNum;
import exceptions.InvalidCliente;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class GestVendasModel implements IGestVendasModel, Serializable {

    private ICatProdutos catProdutos;
    private ICatClientes catClientes;
    private IFaturacao facturacao;
    private List<IFilial> filiais;
    private int nrVendasErradas;
    private String ultimoFicheiroLido;

    /**
     * Caminho para a diretoria onde é gravado o estado do programa caso o utilizador o prentenda.
     */
    private static final String DATABASE_PATH = "data/GestVendas.dat";

    public GestVendasModel() {
        this.facturacao = new Faturacao();
        this.filiais = new ArrayList<>();
        this.nrVendasErradas = 0;
        this.ultimoFicheiroLido = null;

        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            IFilial fl = new Filial();
            filiais.add(fl);
        }
    }

    /**
     * Método que permite criar a base de dados.
     *
     * @param ficheiroClientes Caminho para o ficheiro de Clientes.
     * @param ficheiroProdutos Caminho para o ficheiro de Produtos.
     * @param ficheiroVendas   Caminho para o ficheiro de Vendas.
     */
    public void createData(String ficheiroClientes, String ficheiroProdutos, String ficheiroVendas) {
        this.catClientes = Parse.loadCatClientes(ficheiroClientes);
        this.catProdutos = Parse.loadCatProdutos(ficheiroProdutos);

        BufferedReader inFile;
        String linha;
        IVenda venda;

        try {
            inFile = new BufferedReader(new FileReader(ficheiroVendas));
            while ((linha = inFile.readLine()) != null) {
                venda = Parse.linhaToVenda(linha);
                if (venda != null && venda.isValid() && this.catClientes.existeCliente(venda.getCliente()) && this.catProdutos.existeProduto(venda.getProduto())) {
                    this.filiais.get(venda.getFilial() - 1)
                            .update(venda.getCodigoCliente(), venda.getMes(), venda.getCodigoProduto(), venda.getQuantidade(), venda.getPreco(), venda.getTipo());
                    this.facturacao.update(venda.getFilial(), venda.getMes(), venda.getCodigoProduto(), venda.getQuantidade(), venda.getPreco(), venda.getTipo());
                } else this.nrVendasErradas++;
            }
            this.ultimoFicheiroLido = ficheiroVendas;
        } catch (IOException exc) {
            System.out.println(exc);
        }
    }

    /**
     * Método que permite gravar o estado do programa através de uma stream de objetos.
     *
     * @throws IOException Caso a diretoria não exista.
     */
    public void save() throws IOException {
        Parse.saveObject(this, DATABASE_PATH);
    }

    /**
     * Método que permite recuperar os dados gravados sob a forma de uma stream de objetos para o ficheiro que está em
     * 'DATABASE_PATH'.
     *
     * @return Objeto do tipo `GestVendasModel`
     * @throws ClassNotFoundException Caso ocorra um erro no "cast".
     * @throws IOException            Caso o ficheiro não exista.
     */
    public GestVendasModel load() throws ClassNotFoundException, IOException {
        return (GestVendasModel) Parse.loadObject(DATABASE_PATH);
    }

    /**
     * Método que permite obter o número de vendas erradas.
     *
     * @return Número de vendas erradas.
     */
    public int getNrVendasErradas() {
        return nrVendasErradas;
    }

    public String getUltimoFicheiroLido() {
        return this.ultimoFicheiroLido;
    }

    /**
     * Método que permite obter o número de filiais.
     *
     * @return Número de filiais.
     */
    public int getNumFiliais() {
        return this.filiais.size();
    }

    /**
     * Método que permite obter os códigos dos produtos não transacionados.
     *
     * @return Conjunto dos códigos dos produtos não transacionados.
     */
    public Set<String> getCodigosProdutosNaoComprados() {
        Set<String> codigosProdutos = this.catProdutos.getCodigosProdutos();

        Set<String> codigosProdutosNaoComprados = this.facturacao.getCodigosProdutosNaoComprados(codigosProdutos);

        return codigosProdutosNaoComprados;
    }

    /**
     * Método que permite obter o número total de clientes registados.
     *
     * @return Número total de clientes registados.
     */
    public int getTotalClientes() {
        return this.catClientes.getTotal();
    }

    /**
     * Método que permite obter o número total de produtos registados.
     *
     * @return Número total de produtos registados.
     */
    public int getTotalProdutos() {
        return this.catProdutos.getTotal();
    }

    /**
     * Método que permite obter o número total de produtos não transacionados.
     *
     * @return Número total de produtos não transacionados.
     */
    public int getTotalProdutosNaoComprados() {
        return this.getCodigosProdutosNaoComprados().size();
    }

    /**
     * Método que permite obter o número total de clientes que efetuaram compras.
     *
     * @return Número total de clientes que efetuaram compras.
     */
    public int getTotalClientesCompraram() {
        Set<ICliente> clientes = this.catClientes.getClientes();
        boolean usado;
        int ret = 0;
        for (ICliente cliente : clientes) {
            usado = false;
            for (int i = 0; i < Constantes.N_FILIAIS && !usado; i++)
                try {
                    if (this.filiais.get(i).getNumeroComprasClienteMes(cliente.getCodigo(), 0) > 0)
                        usado = true;
                } catch (InvalidMonth e) {
                    return 0;
                }
            if (usado) ret++;
        }
        return ret;
    }

    /**
     * Método que permite obter o número total de vendas num determindao mês.
     *
     * @param mes Número do mês.
     * @return Número total de vendas nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public int getTotalVendasMes(int mes) throws InvalidMonth {
        return this.facturacao.getTotalVendasMes(mes);
    }

    /**
     * Método que permite obter o número total de vendas num determinado mês numa filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número total de vendas nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public int getTotalVendasMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        return this.facturacao.getTotalVendasMesFilial(mes, filial);
    }

    /**
     * Método que permite obter o total faturado num determinado mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Total faturado nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public double getTotalFaturadoMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        return this.facturacao.getTotalFaturadoMesFilial(mes, filial);
    }

    /**
     * Método que permite obter o total faturado.
     *
     * @return Total faturado.
     */
    public double getTotalFaturado() {
        return this.facturacao.getTotalFaturado();
    }

    /**
     * Método que permite obter o total de clientes que fizeram compras num mês.
     *
     * @param mes Número do mês.
     * @return Número de clientes que fizeram compras nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public int getTotalClientesCompraramMes(int mes) throws InvalidMonth {
        Set<String> totalclientes = new TreeSet<>();
        for (IFilial filial : this.filiais)
            totalclientes.addAll(filial.getCodigosClientesMes(mes));
        return totalclientes.size();
    }

    /**
     * Método que permite obter o número de clientes que fizeram compras numa determinada filial num determinado mês.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número de clientes que realizaram compras nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public int getTotalClientesCompraramMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial {
        if (filial < 1 || filial > Constantes.N_FILIAIS) throw new InvalidFilial("A filial " + filial + " é inválida!");

        return this.filiais.get(filial - 1).getTotalClientesCompraramMes(mes);
    }

    /**
     * Método que permite obter o número de produtos diferentes que um cliente comprou num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Número de produtos diferentes que esse cliente comprou nesse mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public int getProdutosDiferentesMesCliente(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        HashSet<String> todasFiliais = new HashSet<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            todasFiliais.addAll(this.filiais.get(i).getCodigosProdutosCompradosPorCliente(codigoCliente, mes));
        return (todasFiliais.size());
    }

    /**
     * Método que permite determinar o montante gasto por um cliente num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Montante gasto pelo cliente naquele mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public double totalGastoClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        double ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getTotalGastoClienteMes(codigoCliente, mes);
        return ret;
    }

    /**
     * Método que permite obter o total de compras realizadas por um cliente num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Número de compras realizadas pelo cliente naquele mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public int totalComprasClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        int ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getNumeroComprasClienteMes(codigoCliente, mes);
        return ret;
    }

    /**
     * Método que permite determinar por quantos clientes diferentes foi adquirido um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Número de clientes diferentes que adquiriram o produto naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public int getClientesDiferentesMesProduto(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        HashSet<String> todasFiliais = new HashSet<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            todasFiliais.addAll(this.filiais.get(i).getCodigosClientesCompraramProduto(codigoProduto, mes));
        return (todasFiliais.size());
    }

    /**
     * Método que permite determinar quantas vezes foi transacionado um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Número de vezes que o produto foi transacionado naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public int getComprasProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        return this.facturacao.getTotalVendasProdutoMes(codigoProduto, mes);
    }

    /**
     * Método que permite obter a faturação gerada por um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Faturação gerada pelo produto naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public double getFaturacaoProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        return this.facturacao.getTotalFaturadoProdutoMes(codigoProduto, mes);
    }

    /**
     * Método que permite determinar a quantidade que um cliente adquiriu de cada produto.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @return Associação entre cada código de produto e a quantidade adquirida do mesmo pelo cliente.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     */
    public Set<IParStringNum> getProdutosPorQuantidadeCliente(String codigoCliente) throws InvalidCliente {
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        Map<String, Integer> totalCliente = new HashMap<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            for (Map.Entry<String, Integer> par : this.filiais.get(i).getQuantidadesProdutosCliente(codigoCliente).entrySet()) {
                String s = par.getKey();
                if (totalCliente.containsKey(s))
                    totalCliente.put(s, totalCliente.get(s) + par.getValue());
                else totalCliente.put(s, par.getValue());
            }
        }
        Set<IParStringNum> ret = new TreeSet<>(new CompareParValueString());
        for (Map.Entry<String, Integer> par : totalCliente.entrySet())
            ret.add(new ParProdutoQuantidade(par.getKey(), par.getValue()));
        return ret;
    }

    /**
     * Método que permite obter os X clientes que mais adquiriram (em quantidade).
     *
     * @param arg Associação entre cada cliente e a quantidade adquirida.
     * @param x   Número de clientes que pretendemos ordenar em ordem à quantidade adquirida.
     * @return Lista ordenada que associa cada cliente do top X à quantidade adquirida.
     */
    private List<IParStringNum> getFirstXClienteQuantidade(Map<String, Integer> arg, int x) {
        Set<IParStringNum> temp = new TreeSet<>(new CompareParValueString());
        List<IParStringNum> ret = new ArrayList<>();
        for (Map.Entry<String, Integer> par : arg.entrySet())
            temp.add(new ParClienteQuantidade(par.getKey(), par.getValue()));
        int i = 0;
        Iterator<IParStringNum> it = temp.iterator();
        while (it.hasNext() && i < x) {
            ret.add(it.next());
            i++;
        }
        return ret;
    }

    /**
     * Método que permite obter os X produtos mais vendidos.
     *
     * @param x Número de produtos que prentendemos ordenar em ordem à quantidade vendida.
     * @return Lista que associa cada um dos top X produtos à quantidade vendida dos mesmos.
     */
    public List<IParStringNum> getXProdutosMaisVendidosTotal(int x) {
        Map<String, Integer> produtosVendidos = this.facturacao.getProdutosVendidosTotal();
        Set<IParStringNum> pares = new TreeSet<>(new CompareParValueString());
        for (Map.Entry<String, Integer> par : produtosVendidos.entrySet())
            pares.add(new ParProdutoQuantidade(par.getKey(), par.getValue()));
        return pares.stream().limit(x).collect(Collectors.toList());
    }

    /**
     * Método que permite obter os 3 maiores compradores de cada filial.
     *
     * @return Lista de lista que associa cada um dos compradores à quantidade comprada.
     */
    public List<List<IParStringNum>> get3MaioresCompradoresFiliais() {
        List<List<IParStringNum>> ret = new ArrayList<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret.add(this.filiais.get(i).get3MaioresCompradores());
        return ret;
    }

    /**
     * Método que permite obter a lista dos X clientes que compraram mais produtos diferentes.
     *
     * @param x Número de clientes que queremos ordenar em ordem à quantidade de produtos diferentes que adquiriram.
     * @return Lista que associa cada cliente do top X à quantidade de produtos diferentes adquiridos.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public List<IParStringNum> getXClientesMaisProdutosDiferentes(int x) throws InvalidMonth {
        Map<String, Set<String>> totalClientes = new HashMap<>();
        Map<String, Set<String>> clientesFilial;
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            clientesFilial = this.filiais.get(i).getCodigosProdutosCompradosClientesMes(0);
            for (Map.Entry<String, Set<String>> par : clientesFilial.entrySet()) {
                String s = par.getKey();
                if (totalClientes.containsKey(s)) {
                    Set<String> temp = totalClientes.get(s);
                    temp.addAll(par.getValue());
                    totalClientes.put(s, temp);
                } else
                    totalClientes.put(s, par.getValue());
            }
        }
        Map<String, Integer> temp = new HashMap<>();
        for (Map.Entry<String, Set<String>> par : totalClientes.entrySet())
            temp.put(par.getKey(), par.getValue().size());
        return this.getFirstXClienteQuantidade(temp, x);
    }

    /**
     * Método que permite obter a lista dos X clientes que mais compraram um dado produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param x      Número de clientes a ordenar em ordem à quantidade comprada do produto dado.
     * @return Lista que associa cada cliente do top X à quantidade comprada do produto dado.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    public List<IParStringNum> getXClientesMaisCompraramProduto(String codigo, int x) throws InvalidProduto, InvalidMonth {
        if (!this.catProdutos.existeProduto(new Produto(codigo)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        Map<String, Integer> totalCliente = new HashMap<>();
        for (int i = 0; i < Constantes.N_FILIAIS; i++) {
            for (Map.Entry<String, Integer> par : this.filiais.get(i).getNumeroComprasProdutoClientes(codigo, this.filiais.get(i).getCodigosClientesCompraramProduto(codigo, 0)).entrySet()) {
                String s = par.getKey();
                if (totalCliente.containsKey(s))
                    totalCliente.put(s, totalCliente.get(s) + par.getValue());
                else totalCliente.put(s, par.getValue());
            }
        }
        return this.getFirstXClienteQuantidade(totalCliente, x);
    }

    /**
     * Método que permite obter o total gasto por um cliente num dado produto.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param codigoProduto Código do produto a averiguar.
     * @return Montante gasto pelo cliente naquele produto.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     */
    public double getTotalGastoClienteProduto(String codigoCliente, String codigoProduto) throws InvalidProduto, InvalidCliente {
        if (!this.catProdutos.existeProduto(new Produto(codigoProduto)))
            throw new InvalidProduto("O Produto que inseriu não existe!");
        if (!this.catClientes.existeCliente(new Cliente(codigoCliente)))
            throw new InvalidCliente("O Cliente que inseriu não existe!");
        double ret = 0;
        for (int i = 0; i < Constantes.N_FILIAIS; i++)
            ret += this.filiais.get(i).getTotalFaturadoProdutoCliente(codigoProduto, codigoCliente);
        return ret;
    }

    /**
     * Método que permite obter a faturação de cada produto por mês e por filial.
     *
     * @return Associação da faturação de cada produto por mês e por filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    public List<List<List<IParStringNum>>> getFaturacaoProdutosMesFilial() throws InvalidMonth, InvalidFilial {
        List<List<List<IParStringNum>>> ret = new ArrayList<>();
        for (int m = 1; m <= Constantes.N_MESES; m++) {
            List<List<IParStringNum>> temp = new ArrayList<>();
            for (int f = 1; f <= Constantes.N_FILIAIS; f++)
                temp.add(this.facturacao.getTotaisFaturadoFilialMes(f, m));
            ret.add(temp);
        }
        return ret;
    }
}
