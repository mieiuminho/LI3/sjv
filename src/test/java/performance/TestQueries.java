package performance;

import common.Crono;
import exceptions.InvalidCliente;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;
import model.IGestVendasModel;
import model.Parse;
import net.steppschuh.markdowngenerator.table.Table;
import org.junit.*;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.style.Styler;
import performance.models.*;
import view.Color;

import java.io.IOException;
import java.util.*;

import static java.lang.System.out;

// Read more: https://www.vogella.com/tutorials/JUnit/article.html
public class TestQueries {

    private static final String RESOURCES_PATH = "target/classes/";
    private static final String CLIENTS_FILE = RESOURCES_PATH + "Clientes.txt";
    private static final String PRODUCTS_FILE = RESOURCES_PATH + "Produtos.txt";
    private static final String SALES_FILE = RESOURCES_PATH + "Vendas_1M.txt";

    private static Map<String, List<Double>> parsing;
    private static Map<String, List<Double>> queries;

    private IGestVendasModel model;
    private String title;
    private List<Double> parse;
    private List<Double> query;

    @BeforeClass
    public static void setUpBeforeAll() {
        TestQueries.parsing = new HashMap<>();
        TestQueries.queries = new HashMap<>();

        out.println("Ficheiro de Clientes: \t" + TestQueries.CLIENTS_FILE);
        out.println("Ficheiro de Produtos: \t" + TestQueries.PRODUCTS_FILE);
        out.println("Ficheiro de Vendas: \t" + TestQueries.SALES_FILE);
        out.print("\n");
    }

    @AfterClass
    public static void tearDownAfterAll() {
        CategoryChart chart_parsing = new CategoryChartBuilder().width(800).height(600)
                .title("Testes de Performance de Carregamento dos Dados")
                .xAxisTitle("Tarefa").yAxisTitle("Tempo")
                .theme(Styler.ChartTheme.XChart).build();
        List<String> parsing = new ArrayList<>(Arrays.asList(new String[]{"Carregamento dos dados"}));

        for (Map.Entry<String, List<Double>> result : TestQueries.parsing.entrySet()) {
            chart_parsing.addSeries(result.getKey(), parsing, result.getValue());
        }

        CategoryChart chart_queries = new CategoryChartBuilder().width(800).height(600)
                .title("Testes de Performance de resposta às Queries")
                .xAxisTitle("Tarefa").yAxisTitle("Tempo")
                .theme(Styler.ChartTheme.XChart).build();
        List<String> queries = new ArrayList<>(Arrays.asList(new String[]{"Query 5", "Query 6", "Query 7", "Query 8", "Query 9"}));

        for (Map.Entry<String, List<Double>> result : TestQueries.queries.entrySet()) {
            chart_queries.addSeries(result.getKey(), queries, result.getValue());
        }

        try {
            BitmapEncoder.saveBitmapWithDPI(chart_parsing, "./charts/parsing_performance", BitmapEncoder.BitmapFormat.PNG, 300);
            BitmapEncoder.saveBitmapWithDPI(chart_queries, "./charts/queries_performance", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Table.Builder table_parsing = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT, Table.ALIGN_RIGHT)
                .addRow("Modelo", "Leitura");

        for (Map.Entry<String, List<Double>> result : TestQueries.parsing.entrySet()) {
            table_parsing.addRow(result.getKey()
                    , String.format("%.3f", result.getValue().get(0))
            );
        }

        Table.Builder table_queries = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT, Table.ALIGN_RIGHT, Table.ALIGN_RIGHT, Table.ALIGN_RIGHT, Table.ALIGN_RIGHT, Table.ALIGN_RIGHT)
                .addRow("Modelo", "Query 5", "Query 6", "Query 7", "Query 8", "Query 9");

        for (Map.Entry<String, List<Double>> result : TestQueries.queries.entrySet()) {
            table_queries.addRow(result.getKey()
                    , String.format("%.5f", result.getValue().get(0)) // Query 5
                    , String.format("%.3f", result.getValue().get(1)) // Query 6
                    , String.format("%.3f", result.getValue().get(2)) // Query 7
                    , String.format("%.3f", result.getValue().get(3)) // Query 8
                    , String.format("%.3f", result.getValue().get(4)) // Query 9
            );
        }

        try {
            Parse.writeFile("tables/parsing_performance_5M.md", Arrays.asList(new String[]{table_parsing.build().toString()}));
            Parse.writeFile("tables/queries_performance_5M.md", Arrays.asList(new String[]{table_queries.build().toString()}));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp() {
        this.title = "";
        this.model = null;
        this.parse = new ArrayList<>();
        this.query = new ArrayList<>();
    }

    @After
    public void tearDown() {
        TestQueries.queries.put(this.title, new ArrayList<>(this.query));
        TestQueries.parsing.put(this.title, new ArrayList<>(this.parse));
    }

    @Test
    public void testModelHashHashVector() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "HashMap -- HashSet -- Vector";
        this.model = new ModelHashHashVector();

        this.queries();
    }

    @Test
    public void testModelHashTreeVector() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "HashMap -- TreeSet -- Vector";
        this.model = new ModelHashTreeVector();

        this.queries();
    }

    @Test
    public void testModelHashHashArray() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "HashMap -- HashSet -- ArrayList";
        this.model = new ModelHashHashArray();

        this.queries();
    }

    @Test
    public void testModelHashTreeArray() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "HashMap -- TreeSet -- ArrayList";
        this.model = new ModelHashTreeArray();

        this.queries();
    }

    @Test
    public void testModelTreeHashVector() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "TreeMap -- HashSet -- Vector";
        this.model = new ModelTreeHashVector();

        this.queries();
    }

    @Test
    public void testModelTreeTreeVector() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "TreeMap -- TreeSet -- Vector";
        this.model = new ModelTreeTreeVector();

        this.queries();
    }

    @Test
    public void testModelTreeHashArray() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "TreeMap -- HashSet -- ArrayList";
        this.model = new ModelTreeHashArray();

        this.queries();
    }

    @Test
    public void testModelTreeTreeArray() throws InvalidCliente, InvalidProduto, InvalidMonth {
        this.title = "TreeMap -- TreeSet -- ArrayList";
        this.model = new ModelTreeTreeArray();

        this.queries();
    }

    public void queries() throws InvalidCliente, InvalidProduto, InvalidMonth {
        out.println(Color.ANSI_BLUE + this.title + Color.ANSI_RESET);

        // Criar modelo
        Crono.start();
        this.model.createData(CLIENTS_FILE, PRODUCTS_FILE, SALES_FILE);
        Crono.stop();
        this.parse.add(Crono.time());
        out.println("Leitura: " + Crono.time() + " segundos");

        // Query 5
        Crono.start();
        this.model.getProdutosPorQuantidadeCliente("F2916");
        Crono.stop();
        this.query.add(Crono.time());
        out.println("Query 5: " + Crono.time() + " segundos");

        // Query 6
        Crono.start();
        this.model.getXProdutosMaisVendidosTotal(10);
        Crono.stop();
        this.query.add(Crono.time());
        out.println("Query 6: " + Crono.time() + " segundos");

        // Query 7
        Crono.start();
        this.model.get3MaioresCompradoresFiliais();
        Crono.stop();
        this.query.add(Crono.time());
        out.println("Query 7: " + Crono.time() + " segundos");

        // Query 8
        Crono.start();
        this.model.getXClientesMaisProdutosDiferentes(10);
        Crono.stop();
        this.query.add(Crono.time());
        out.println("Query 8: " + Crono.time() + " segundos");

        // Query 9
        Crono.start();
        this.model.getXClientesMaisCompraramProduto("FD1083", 10);
        Crono.stop();
        this.query.add(Crono.time());
        out.println("Query 9: " + Crono.time() + " segundos");

        out.print("\n");
    }
}
