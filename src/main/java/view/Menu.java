package view;

import org.apache.commons.lang3.StringUtils;

import static java.lang.System.out;

public class Menu {
    private String title;
    private String[] options;

    public Menu(String title, String[] options) {
        this.title = title;
        this.options = new String[options.length];

        for (int i = 0; i < options.length; i++) {
            this.options[i] = options[i];
        }
    }

    public void show() {
        out.print(Color.ANSI_GREEN);
        out.println(StringUtils.center("▂▃▄▅▆▇ " + this.title.toUpperCase() + " ▇▆▅▄▃▂", Terminal.WINDOW_WIDTH));
        out.print(Color.ANSI_RESET);

        System.out.println(StringUtils.center(
                StringUtils.rightPad(" ╔", Terminal.TEXT_MAX_WIDTH, "═") + "╗ ", Terminal.WINDOW_WIDTH)
        );

        for (int i = 0; i < this.options.length; i++) {
            if (i == 0) {
                System.out.println(StringUtils.center(
                        StringUtils.rightPad("║ " + ((i + 1) % this.options.length) + ". " + this.options[i], Terminal.TEXT_MAX_WIDTH - 2) + " ║", Terminal.WINDOW_WIDTH)
                );
            } else {
                System.out.println(StringUtils.center(
                        StringUtils.rightPad("╠", Terminal.TEXT_MAX_WIDTH - 1, "═") + "╣", Terminal.WINDOW_WIDTH)
                );
                System.out.println(StringUtils.center(
                        StringUtils.rightPad("║ " + ((i + 1) % this.options.length) + ". " + this.options[i], Terminal.TEXT_MAX_WIDTH - 2) + " ║", Terminal.WINDOW_WIDTH)
                );
            }
        }
        System.out.println(StringUtils.center(
                StringUtils.rightPad(" ╚", Terminal.TEXT_MAX_WIDTH, "═") + "╝ ", Terminal.WINDOW_WIDTH)
        );
    }

    public void list() {
        out.println(StringUtils.center(
                StringUtils.rightPad(Color.ANSI_CYAN + title.toUpperCase() + Color.ANSI_RESET, Terminal.TEXT_MAX_WIDTH), Terminal.WINDOW_WIDTH)
        );

        out.print("\n");

        String list = "";

        for (int i = 0; i < options.length; i++) {
            list += " [" + Color.ANSI_BLUE + ((i + 1) % this.options.length) + Color.ANSI_RESET + "] " + this.options[i] + "  ";
        }

        out.println(StringUtils.center(
                StringUtils.rightPad(list, Terminal.TEXT_MAX_WIDTH), Terminal.WINDOW_WIDTH)
        );

        out.print("\n");
    }
}

