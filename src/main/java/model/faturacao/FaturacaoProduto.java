package model.faturacao;

import java.io.Serializable;

public class FaturacaoProduto implements Serializable {
    private String codigoProduto;
    private double faturadoN;
    private double faturadoP;
    private int unidadesN;
    private int unidadesP;
    private int registosVendaN;
    private int registosVendaP;

    public FaturacaoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
        this.faturadoN = 0;
        this.faturadoP = 0;
        this.unidadesN = 0;
        this.unidadesP = 0;
        this.registosVendaN = 0;
        this.registosVendaP = 0;
    }

    public FaturacaoProduto(FaturacaoProduto outro) {
        this.codigoProduto = outro.codigoProduto;
        this.faturadoN = outro.faturadoN;
        this.faturadoP = outro.faturadoP;
        this.unidadesN = outro.unidadesN;
        this.unidadesP = outro.unidadesP;
        this.registosVendaN = outro.registosVendaN;
        this.registosVendaP = outro.registosVendaP;
    }

    public void update(int quantidade, double valor, String tipo) {
        if (tipo.equals("P")) {
            this.faturadoP += valor;
            this.unidadesP += quantidade;
            this.registosVendaP++;
        } else {
            this.faturadoN += valor;
            this.unidadesN += quantidade;
            this.registosVendaN++;
        }
    }

    public String getCodigoProduto() {
        return this.codigoProduto;
    }

    public double getFaturadoN() {
        return this.faturadoN;
    }

    public double getFaturadoP() {
        return this.faturadoP;
    }

    public double getTotalFaturado() {
        return this.faturadoN + this.faturadoP;
    }

    public int getUnidadesN() {
        return this.unidadesN;
    }

    public int getUnidadesP() {
        return this.unidadesP;
    }

    public int getTotalUnidades() {
        return this.unidadesN + this.unidadesP;
    }

    public int getRegistosVendaN() {
        return this.registosVendaN;
    }

    public int getRegistosVendaP() {
        return this.registosVendaP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FaturacaoProduto that = (FaturacaoProduto) o;

        if (Double.compare(that.faturadoN, faturadoN) != 0) return false;
        if (Double.compare(that.faturadoP, faturadoP) != 0) return false;
        if (unidadesN != that.unidadesN) return false;
        if (unidadesP != that.unidadesP) return false;
        if (registosVendaN != that.registosVendaN) return false;
        if (registosVendaP != that.registosVendaP) return false;
        return codigoProduto != null ? codigoProduto.equals(that.codigoProduto) : that.codigoProduto == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = codigoProduto != null ? codigoProduto.hashCode() : 0;
        temp = Double.doubleToLongBits(faturadoN);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(faturadoP);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + unidadesN;
        result = 31 * result + unidadesP;
        result = 31 * result + registosVendaN;
        result = 31 * result + registosVendaP;
        return result;
    }

    @Override
    public FaturacaoProduto clone() {
        return new FaturacaoProduto(this);
    }

    @Override
    public String toString() {
        return "FaturacaoProduto {" +
                "codigoProduto='" + codigoProduto + '\'' +
                ", faturadoN=" + faturadoN +
                ", faturadoP=" + faturadoP +
                ", unidadesN=" + unidadesN +
                ", unidadesP=" + unidadesP +
                ", registosVendaN=" + registosVendaN +
                ", registosVendaP=" + registosVendaP +
                "}";
    }
}
