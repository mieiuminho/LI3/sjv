package common;

public interface ITripleStringNumNum {

    String getKey();

    Number getFirst();

    Number getSecond();
}
