package unity;

import model.Cliente;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestCliente {
    private Cliente cliente;

    @Before
    public void setUp() {
        cliente = new Cliente("J3103");
    }

    @Test
    public void testEquality() {
        Assert.assertEquals("J3103", cliente.getCodigo());
    }
}
