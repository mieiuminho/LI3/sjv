package common;

import java.io.Serializable;

public class ParStringNum implements Serializable, IParStringNum {
    private String string;
    private Number number;

    public ParStringNum(String string, Number number){
        this.string = string;
        this.number = number;
    }

    @Override
    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public Number getNumber() {
        return number;
    }
}
