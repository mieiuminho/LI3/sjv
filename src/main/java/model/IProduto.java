package model;

public interface IProduto {

    /**
     * Método que permite obter o código do produto.
     *
     * @return String com o código do produto.
     */
    String getCodigo();

    /**
     * Método que permite definir o código do produto.
     *
     * @param codigo String como o código do produto.
     */
    void setCodigo(String codigo);

    /**
     * Método que permite obter a cópia de um objeto de um classe que satisfaça a interface `IProduto`
     *
     * @return Cópia do objeto
     */
    IProduto clone();

    /**
     * Método que permite determinar se dois objetos são equivalentes.
     *
     * @param o Objeto a comparar.
     * @return `true` caso afirmativo ou `false` caso contrário.
     */
    boolean equals(Object o);
}
