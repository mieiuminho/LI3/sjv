package model;

import java.util.List;

public final class Constantes {
    /**
     * Caminho do ficheiro de configurações.
     */
    public static final String CONFIGURATION_FILE = "data/configs.txt";

    /**
     * Número de Filiais.
     */
    public static int N_FILIAIS;

    /**
     * Número de meses do ano.
     */
    public static int N_MESES;

    /**
     * Caminho da pasta 'resources' do projeto.
     */
    public static String RESOURCES_PATH;

    /**
     * Caminho do ficheiro de Clientes.
     */
    public static String FILE_CLIENTES;

    /**
     * Caminho do ficheiro de Produtos.
     */
    public static String FILE_PRODUTOS;

    /**
     * Caminho do ficheiro de Vendas.
     */
    public static String FILE_VENDAS;

    static {
        List<String> lines = Parse.readFile(Constantes.CONFIGURATION_FILE);

        for (String line : lines) {
            String[] fields = line.split(":");
            try {
                switch (fields[0]) {
                    case "Filiais":
                        Constantes.N_FILIAIS = Integer.parseInt(fields[1].trim());
                        break;
                    case "Meses":
                        Constantes.N_MESES = Integer.parseInt(fields[1].trim());
                        break;
                    case "Resources Path":
                        if (fields.length > 1)
                            Constantes.RESOURCES_PATH = fields[1].trim();
                        else
                            Constantes.RESOURCES_PATH = "";
                        break;
                    case "Ficheiro Clientes":
                        Constantes.FILE_CLIENTES = RESOURCES_PATH + fields[1].trim();
                        break;
                    case "Ficheiro Produtos":
                        Constantes.FILE_PRODUTOS = RESOURCES_PATH + fields[1].trim();
                        break;
                    case "Ficheiro Vendas":
                        Constantes.FILE_VENDAS = RESOURCES_PATH + fields[1].trim();
                        break;
                    default:
                        System.err.println("\nERRO NAS CONFIGS: " + line);
                }
            } catch (Exception e) {
                System.err.println("\nERRO NAS CONFIGS: " + line);
                System.err.println(e.getMessage());
                System.exit(1);
            }
        }
    }
}
