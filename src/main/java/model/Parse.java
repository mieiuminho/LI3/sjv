package model;

import java.io.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class Parse {
    public static List<String> readFile(String file) {
        List<String> lines = new ArrayList<>();
        BufferedReader inFile;
        String line;

        try {
            inFile = new BufferedReader(new FileReader(file));
            while ((line = inFile.readLine()) != null) lines.add(line);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return lines;
    }

    public static void writeFile(String filename, List<String> lines) throws IOException {
            PrintWriter file = new PrintWriter(filename);
            for(String line : lines) {
                file.println(line);
            }
            file.flush();
            file.close();
    }

    public static List<IVenda> loadVendas(String ficheiro, ICatClientes catcli, ICatProdutos catprod) {
        List<IVenda> vendas = new ArrayList<>();
        BufferedReader inFile;
        String linha;
        IVenda venda;

        try {
            inFile = new BufferedReader(new FileReader(ficheiro));
            while ((linha = inFile.readLine()) != null) {
                venda = Parse.linhaToVenda(linha);
                if (venda != null && venda.isValid() && catcli.existeCliente(venda.getCliente()) && catprod.existeProduto(venda.getProduto())) {
                    vendas.add(venda);
                }
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return vendas;
    }

    public static IVenda linhaToVenda(String linha) {
        String codPro, codCli, tipo;
        int mes;
        int filial;
        int quantidade;
        double preco;
        String[] campos = linha.split(" ");
        /* 0 codigo produto, 1 preço, 2 quantidade, 3 tipo de venda, 4 codigo cliente, 5 mes, 6 filial */
        codPro = campos[0];
        codCli = campos[4];
        tipo = campos[3];

        try {
            preco = Double.parseDouble(campos[1]);
            quantidade = Integer.parseInt(campos[2]);
            mes = Integer.parseInt(campos[5]);
            filial = Integer.parseInt(campos[6]);
        } catch (InputMismatchException exc) {
            return null;
        }

        return new Venda(codPro, preco, quantidade, tipo, codCli, mes, filial);
    }

    public static ICatClientes loadCatClientes(String ficheiro) {
        ICatClientes catcli = new CatClientes();
        BufferedReader inFile;
        String linha;
        ICliente cliente;

        try {
            inFile = new BufferedReader(new FileReader(ficheiro));
            while ((linha = inFile.readLine()) != null) {
                cliente = Parse.linhaToCliente(linha);
                if (cliente != null) catcli.insereCliente(cliente);
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return catcli;
    }

    public static ICatClientes loadCatClientesTree(String ficheiro) {
        ICatClientes catcli = new CatClientesTree();
        BufferedReader inFile;
        String linha;
        ICliente cliente;

        try {
            inFile = new BufferedReader(new FileReader(ficheiro));
            while ((linha = inFile.readLine()) != null) {
                cliente = Parse.linhaToCliente(linha);
                if (cliente != null) catcli.insereCliente(cliente);
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return catcli;
    }

    private static ICliente linhaToCliente(String linha) {
        String[] campos = linha.split("\\r?\\n");
        ICliente cliente = null;

        if (campos[0].matches("^[A-Z](5000|[1-4][0-9][0-9][0-9])$")) {
            cliente = new Cliente(campos[0]);
        }

        return cliente;
    }

    public static ICatProdutos loadCatProdutos(String ficheiro) {
        ICatProdutos catpro = new CatProdutos();
        BufferedReader inFile;
        String linha;
        IProduto produto;

        try {
            inFile = new BufferedReader(new FileReader(ficheiro));
            while ((linha = inFile.readLine()) != null) {
                produto = Parse.linhaToProduto(linha);
                if (produto != null) catpro.insereProduto(produto);
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return catpro;
    }

    public static ICatProdutos loadCatProdutosTree(String ficheiro) {
        ICatProdutos catpro = new CatProdutosTree();
        BufferedReader inFile;
        String linha;
        IProduto produto;

        try {
            inFile = new BufferedReader(new FileReader(ficheiro));
            while ((linha = inFile.readLine()) != null) {
                produto = Parse.linhaToProduto(linha);
                if (produto != null) catpro.insereProduto(produto);
            }
        } catch (IOException exc) {
            System.out.println(exc);
        }

        return catpro;
    }

    private static IProduto linhaToProduto(String linha) {
        String[] campos = linha.split("\\r?\\n");
        IProduto produto = null;

        if (campos[0].matches("^[A-Z][A-Z][1-9][0-9][0-9][0-9]$")) {
            produto = new Produto(campos[0]);
        }

        return produto;
    }

    public static void saveObject(Object object, String file) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(object);
    }

    public static Object loadObject(String file) throws ClassNotFoundException, IOException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        Object obj = in.readObject();
        return obj;
    }
}
