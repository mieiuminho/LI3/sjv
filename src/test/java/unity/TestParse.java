package unity;

import model.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.lang.System.out;

public class TestParse {
    private ICatClientes catcli;
    private ICatProdutos catprod;
    private List<IVenda> vendas;

    @Test
    public void readVendas() {
        this.catcli = Parse.loadCatClientes("target/test-classes/Clientes_Teste.txt");
        this.catprod = Parse.loadCatProdutos("target/test-classes/Produtos_Teste.txt");
        this.vendas = Parse.loadVendas("target/test-classes/Vendas_Teste.txt", catcli, catprod);
        Assert.assertEquals(11, vendas.size());
    }

}
