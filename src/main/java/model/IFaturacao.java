package model;

import common.IParStringNum;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IFaturacao {

    /**
     * Método que permite atualizar a estrutura `Faturacao`.
     *
     * @param filial        Número da filial.
     * @param mes           Número do mês.
     * @param codigoProduto Código do produto transacionado.
     * @param quantidade    Quantidade transacionada.
     * @param preco         Preço do produto transacionado.
     * @param tipo          Regime da transação (Promoção / Normal).
     */
    void update(int filial, int mes, String codigoProduto, int quantidade, double preco, String tipo);

    /**
     * Método que permite obter os códigos dos produtos não transacionados.
     *
     * @param codigos Conjunto dos códigos de produto registados.
     * @return Conjunto dos códigos dos produtos não transacionados.
     */
    Set<String> getCodigosProdutosNaoComprados(Set<String> codigos);

    /**
     * Método que permite obter o total de um vendas efetuadas num mês.
     *
     * @param mes Número do mês.
     * @return Número total de vendas efetuadas nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido
     */
    int getTotalVendasMes(int mes) throws InvalidMonth;

    /**
     * Método que permite obter o total de vendas efetuada num mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número total de compras efetuadas nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    int getTotalVendasMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial;

    /**
     * Método que permite obter o total de vendas num mês de um determinado produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param mes    Número do mês.
     * @return Total de vendas nesse mês desse produto.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    int getTotalVendasProdutoMes(String codigo, int mes) throws InvalidMonth;

    /**
     * Método que permite obter o total faturado com as vendas num mês de um determiando produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param mes    Número do mês.
     * @return Total faturado com as vendas desse produto nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    double getTotalFaturadoProdutoMes(String codigo, int mes) throws InvalidMonth;

    /**
     * Método que permite obter o total faturado num mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Total faturado nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    double getTotalFaturadoMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial;

    /**
     * Método que permite obter o total faturado.
     *
     * @return Total faturado.
     */
    double getTotalFaturado();

    /**
     * Método que permite obter a correspondência entre um produto e o seu número total de vendas.
     *
     * @return Correspondência entre um código de produto e o seu número total de vendas.
     */
    Map<String, Integer> getProdutosVendidosTotal();

    /**
     * Método que permite o obter o total faturado numa filial por mês.
     *
     * @param filial Número da filial.
     * @param mes    Número do mês.
     * @return Correspondência entre filial, mês e o total faturado.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    List<IParStringNum> getTotaisFaturadoFilialMes(int filial, int mes) throws InvalidMonth, InvalidFilial;
}
