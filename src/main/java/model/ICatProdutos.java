package model;

import java.util.Set;

public interface ICatProdutos {

    /**
     * Método que permite obter o número total de produtos.
     *
     * @return Número total de produtos.
     */
    int getTotal();

    /**
     * Método que permite obter o número total de produtos envolvidos em transações.
     *
     * @return Número total de produtos envolvidos em transações.
     */
    int getUsados();

    /**
     * Método que permite obter o número total de produtos não envolvidos em transações.
     *
     * @return Número total de produtos não envolvidos em transações.
     */
    int getNaoUsados();

    /**
     * Método que permite obter os produtos registados.
     *
     * @return `Set` que contém os produtos registados.
     */
    Set<IProduto> getProdutos();

    /**
     * Método que permite obter os códigos de todos os produtos registados.
     *
     * @return `Set` que contém os códigos de todos os produtos registados.
     */
    Set<String> getCodigosProdutos();

    /**
     * Método que permite inserir um produto na estrutura de produtos.
     *
     * @param produto Produto a inserir.
     */
    void insereProduto(IProduto produto);

    /**
     * Método que permite determinar se um produto está ou não registado.
     *
     * @param produto Produto a averiguar.
     * @return `true` caso o produto esteja registado ou `false` caso contrário.
     */
    boolean existeProduto(IProduto produto);
}
