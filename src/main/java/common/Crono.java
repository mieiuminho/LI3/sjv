package common;

import static java.lang.System.nanoTime;

/**
 * Crono = mede um tempo entre start() e stop().
 * O tempo e medido em nanosegundos e convertido para segundos.
 *
 * @author FMM
 * @version 20190427
 */
public class Crono {

    private static long inicio = 0L;
    private static long fim = 0L;
    private static double time = 0;

    public static void start() {
        fim = 0L;
        time = 0;
        inicio = nanoTime();
    }

    public static void stop() {
        fim = nanoTime();
        // em segundos
        time = (fim - inicio) / 1.0E09;
    }

    public static double time() {
        return time;
    }

    public static String toShow() {
        return "Tempo: " + time + " segundos";
    }
}
