package common;

import java.io.Serializable;

public class TripleStringNumberNumber implements Serializable, ITripleStringNumNum {
    private String key;
    private Number first;
    private Number second;

    public TripleStringNumberNumber(String key, Number first, Number second) {
        this.key = key;
        this.first = first;
        this.second = second;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public Number getFirst() {
        return this.first;
    }

    @Override
    public Number getSecond() {
        return this.second;
    }
}
