package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class CatProdutos implements ICatProdutos, Serializable {

    private Set<IProduto> produtos;
    private int usados;

    /**
     * Construtor não parametrizado.
     */
    public CatProdutos() {
        this.produtos = new HashSet<>();
        this.usados = 0;
    }

    /**
     * Método que permite obter o número total de produtos.
     *
     * @return Número total de produtos.
     */
    public int getTotal() {
        return this.produtos.size();
    }

    /**
     * Método que permite obter o número total de produtos envolvidos em transações.
     *
     * @return Número total de produtos envolvidos em transações.
     */
    public int getUsados() {
        return this.usados;
    }

    /**
     * Método que permite obter o número total de produtos não envolvidos em transações.
     *
     * @return Número total de produtos não envolvidos em transações.
     */
    public int getNaoUsados() {
        return this.getTotal() - this.getUsados();
    }

    /**
     * Método que permite obter os produtos registados.
     *
     * @return `Set` que contém os produtos registados.
     */
    public Set<IProduto> getProdutos() {
        Set<IProduto> lst = new TreeSet<>();

        for (IProduto prd : this.produtos) {
            lst.add(prd.clone());
        }

        return lst;
    }

    /**
     * Método que permite obter os códigos de todos os produtos registados.
     *
     * @return `Set` que contém os códigos de todos os produtos registados.
     */
    public Set<String> getCodigosProdutos() {
        Set<String> result = new TreeSet<>();

        for (IProduto prod : this.produtos) {
            result.add(prod.getCodigo());
        }

        return result;
    }

    /**
     * Método que permite inserir um produto na estrutura de produtos.
     *
     * @param produto Produto a inserir.
     */
    public void insereProduto(IProduto produto) {
        this.produtos.add(produto.clone());
    }

    /**
     * Método que permite determinar se um produto está ou não registado.
     *
     * @param produto Produto a averiguar.
     * @return `true` caso o produto esteja registado ou `false` caso contrário.
     */
    public boolean existeProduto(IProduto produto) {
        return this.produtos.contains(produto);
    }
}
