package model;

import common.IParStringNum;

import java.io.Serializable;

public class ParProdutoQuantidade implements Serializable, IParStringNum {
    private String codigoProduto;
    private int quantidade;

    public ParProdutoQuantidade(String codigoProduto, int quantidade) {
        this.codigoProduto = codigoProduto;
        this.quantidade = quantidade;
    }

    @Override
    public void setString(String string) {
        this.codigoProduto = string;
    }

    @Override
    public String getString() {
        return this.codigoProduto;

    }

    @Override
    public Number getNumber() {
        return this.quantidade;
    }
}
