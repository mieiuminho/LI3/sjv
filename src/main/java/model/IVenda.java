package model;

public interface IVenda {

    /**
     * Método que permite obter uma cópia do produto.
     *
     * @return Cópia do produto.
     */
    IProduto getProduto();

    /**
     * Método que permite obter o código do produto.
     *
     * @return String com o código do produto.
     */
    String getCodigoProduto();

    /**
     * Método que permite obter o preço do produto.
     *
     * @return Preço do produto.
     */
    double getPreco();

    /**
     * Método que permite obter a quantidade transacionada.
     *
     * @return Quantidade transacionada.
     */
    int getQuantidade();

    /**
     * Método que permite obter o regime em que aconteceu a transação (Promoção / Normal).
     *
     * @return Regime em que ocorreu a transação.
     */
    String getTipo();

    /**
     * Método que permite obter o cliente autor da transação.
     *
     * @return Cliente autor da transação.
     */
    ICliente getCliente();

    /**
     * Método que permite obter o código do cliente autor da transação.
     *
     * @return Código do cliente autor da transação.
     */
    String getCodigoCliente();

    /**
     * Método que permite obter o mês em que ocorreu a transação.
     *
     * @return Mês em que ocorreu a transação.
     */
    int getMes();

    /**
     * Método que permite obter a filial em que ocorreu a transação.
     *
     * @return Filial em que ocorreu a transação.
     */
    int getFilial();

    /**
     * Método que permite obter o valor transacionado.
     *
     * @return Valor transacionado.
     */
    double getValor();

    /**
     * Método que determina se uma venda é válida.
     *
     * @return `true` caso afirmativo ou `false` caso contrário.
     */
    boolean isValid();

    /**
     * Método que permite obter uma cópia do objeto que satisfaça a interface `IVenda`.
     *
     * @return Cópia do objeto.
     */
    IVenda clone();

    /**
     * Método que permite obter uma representação textual de um objeto do tipo `Venda`.
     *
     * @return Representação textual do objeto.
     */
    String toString();
}
