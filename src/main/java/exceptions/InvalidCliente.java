package exceptions;

public class InvalidCliente extends Exception {

    public InvalidCliente(String message) {
        super(message);
    }
}
