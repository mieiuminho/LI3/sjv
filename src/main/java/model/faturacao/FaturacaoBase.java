package model.faturacao;

import model.Faturacao;

import java.io.Serializable;
import java.util.*;

public class FaturacaoBase implements Serializable {
    private double totalFaturado;
    private int totalVendas; // número de linhas de venda
    private Map<String, FaturacaoProduto> produtos;

    public FaturacaoBase() {
        this.totalFaturado = 0;
        this.totalVendas = 0;
        this.produtos = new HashMap<>();
    }

    public FaturacaoBase(FaturacaoBase fatbase) {
        this.totalFaturado = fatbase.getTotalFaturado();
        this.totalVendas = fatbase.getTotalVendas();
        this.produtos = new HashMap<>();
        fatbase.produtos.forEach((k, v) -> this.produtos.put(k, v.clone()));
    }

    public void update(String codigoProduto, int quantidade, double preco, String tipo) {
        double valor = quantidade * preco;
        this.totalFaturado += valor;
        this.totalVendas++;

        if (this.produtos.containsKey(codigoProduto)) {
            this.produtos.get(codigoProduto).update(quantidade, valor, tipo);
        } else {
            FaturacaoProduto fatprod = new FaturacaoProduto(codigoProduto);
            fatprod.update(quantidade, valor, tipo);
            this.produtos.put(codigoProduto, fatprod);
        }
    }

    public double getTotalFaturado() {
        return this.totalFaturado;
    }

    public int getTotalVendas() {
        return this.totalVendas;
    }

    public boolean exiteProduto(String codigoProduto) {
        return this.produtos.containsKey(codigoProduto);
    }

    public int getTotalVendasProduto(String codigoProduto) {
        if (exiteProduto(codigoProduto))
            return this.produtos.get(codigoProduto).getTotalUnidades();
        else return 0;
    }

    public double getTotalFaturadoProduto(String codigoProduto) {
        if (exiteProduto(codigoProduto))
            return (this.produtos.get(codigoProduto).getFaturadoN() + this.produtos.get(codigoProduto).getFaturadoP());
        else return 0;
    }

    public Set<FaturacaoProduto> getFaturacaoProdutos() {
        Set<FaturacaoProduto> ret = new HashSet<>();
        for (FaturacaoProduto fp : this.produtos.values())
            ret.add(fp.clone());
        return ret;
    }

    @Override
    public FaturacaoBase clone() {
        return new FaturacaoBase(this);
    }

    @Override
    public String toString() {
        return "FaturacaoBase {" +
                "totalFaturado=" + totalFaturado +
                ", totalVendas=" + totalVendas +
                ", produtos=" + produtos +
                '}';
    }
}
