package exceptions;

public class InvalidFilial extends Exception {

    public InvalidFilial(String message) {
        super(message);
    }
}
