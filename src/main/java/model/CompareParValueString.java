package model;

import common.IParStringNum;

import java.util.Comparator;

public class CompareParValueString implements Comparator<IParStringNum> {

    public int compare(IParStringNum a, IParStringNum b) {
        if (a.getNumber().doubleValue() > b.getNumber().doubleValue())
            return -1;
        if (a.getNumber().doubleValue() < b.getNumber().doubleValue())
            return 1;
        else return a.getString().compareTo(b.getString());
    }
}
