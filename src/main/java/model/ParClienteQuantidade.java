package model;

import common.IParStringNum;

import java.io.Serializable;

public class ParClienteQuantidade implements Serializable, IParStringNum {
    private String codigoCliente;
    private int quantidade;

    public ParClienteQuantidade(String codigoCliente, int quantidade) {
        this.codigoCliente = codigoCliente;
        this.quantidade = quantidade;
    }

    @Override
    public void setString(String string) {
        this.codigoCliente = string;
    }

    @Override
    public String getString() {
        return this.codigoCliente;
    }

    @Override
    public Number getNumber() {
        return this.quantidade;
    }
}
