package unity;

import exceptions.InvalidCliente;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;
import model.GestVendasModel;
import model.IGestVendasModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static java.lang.System.out;

public class TestGereVendasModel {
    private IGestVendasModel model;

    @Before
    public void setUp() {
        this.model = new GestVendasModel();
        this.model.createData("target/test-classes/Clientes_Teste.txt", "target/test-classes/Produtos_Teste.txt", "target/test-classes/Vendas_Teste.txt");
    }

    @Test
    public void getCodigosProdutosNaoComprados() {
        Set<String> codigosProdutosNaoComprados = this.model.getCodigosProdutosNaoComprados();
        out.println("Produtos nunca comprados: ");

        for (String codigo : codigosProdutosNaoComprados) {
            out.println(codigo);
        }

        Assert.assertEquals(1, codigosProdutosNaoComprados.size());
    }

    @Test
    public void getNrVendasErradas() {
        int nrVendasErradas = this.model.getNrVendasErradas();
        Assert.assertEquals(10, nrVendasErradas);
    }

    @Test
    public void totalClientes() {
        int totalClientes = this.model.getTotalClientes();
        Assert.assertEquals(10, totalClientes);
    }

    @Test
    public void totalProdutos() {
        int totalProdutos = model.getTotalProdutos();
        Assert.assertEquals(10, totalProdutos);
    }

    @Test
    public void totalClientesCompraram() {
        int totalClientesCompraram = model.getTotalClientesCompraram();
        Assert.assertEquals(9, totalClientesCompraram);

    }

    @Test
    public void getTotalVendasMes() throws InvalidMonth {
        int totalVendasMes = this.model.getTotalVendasMes(3);
        Assert.assertEquals(3, totalVendasMes);
    }

    @Test
    public void getTotalVendasMesFilial() throws InvalidMonth, InvalidFilial {
        int totalVendasMesFilial = this.model.getTotalVendasMesFilial(3, 1);
        Assert.assertEquals(1, totalVendasMesFilial);
    }

    @Test
    public void getTotalFaturadoMesFilial() throws InvalidMonth, InvalidFilial {
        double totalFaturadoMesFilial = this.model.getTotalFaturadoMesFilial(3, 1);
        Assert.assertEquals(2048.47, totalFaturadoMesFilial, 0.01);
    }

    @Test
    public void getTotalFaturado() {
        double totalFaturado = this.model.getTotalFaturado();
        Assert.assertEquals(496841.87, totalFaturado, 0.01);

    }

    @Test
    public void getTotalClientesCompraramMes() throws InvalidMonth {
        int clientesCompraramMes = this.model.getTotalClientesCompraramMes(3);
        Assert.assertEquals(2, clientesCompraramMes);
    }

    @Test
    public void getTotalClientesCompraramMesFilial() throws InvalidMonth, InvalidFilial {
        int clientesCompraramMesFilial = this.model.getTotalClientesCompraramMesFilial(3, 1);
        Assert.assertEquals(1, clientesCompraramMesFilial);
    }

    @Test
    public void getProdutosDiferentesMesCliente() throws InvalidCliente, InvalidMonth {
        int produtosDiferentesMesClientes = this.model.getProdutosDiferentesMesCliente("L4891", 3);
        Assert.assertEquals(0, produtosDiferentesMesClientes);
    }

    @Test
    public void totalGastoClienteMes() throws InvalidCliente, InvalidMonth {
        double totalGastoClienteMes = this.model.totalGastoClienteMes("L4891", 2);
        Assert.assertEquals(9948.16, totalGastoClienteMes, 0.01);
    }

    @Test
    public void totalComprasClienteMes() throws InvalidCliente, InvalidMonth {
        int totalComprasClienteMes = this.model.totalComprasClienteMes("L4891", 2);
        Assert.assertEquals(1, totalComprasClienteMes);
    }

    @Test
    public void getClientesDiferentesMesProduto() throws InvalidMonth, InvalidProduto {
        int clientesDiferentesMesProduto = this.model.getClientesDiferentesMesProduto("TG1654", 3);
        Assert.assertEquals(1, clientesDiferentesMesProduto);
    }

    @Test
    public void getComprasProdutoMes() throws InvalidProduto, InvalidMonth {
        int comprasProdutoMes = this.model.getComprasProdutoMes("KR1583", 2);
        Assert.assertEquals(128, comprasProdutoMes);
    }

    @Test
    public void getFaturacaoProdutoMes() throws InvalidProduto, InvalidMonth {
        double faturacaoProdutoMes = this.model.getFaturacaoProdutoMes("OP1244", 9);
        Assert.assertEquals(33691.62, faturacaoProdutoMes, 0.01);
    }
}
