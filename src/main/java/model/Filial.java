package model;

import common.IParStringNum;
import exceptions.InvalidMonth;
import model.filial.ClienteProdutos;
import model.filial.InfoProduto;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Filial implements IFilial, Serializable {
    // Deve ter 13 posições no array, na 0 encontram-se os registo anuais (totais)
    private List<Map<String, ClienteProdutos>> registos;

    /**
     * Construtor não parametrizado.
     */
    public Filial() {
        this.registos = new ArrayList<>(Constantes.N_MESES + 1);
        for (int i = 0; i < Constantes.N_MESES + 1; i++) {
            Map<String, ClienteProdutos> map = new HashMap<>();
            this.registos.add(map);
        }
    }

    /**
     * Métoodo que permite atualizar a estrutura `Filial`.
     *
     * @param codigoCliente Código do cliente envolvido na transação.
     * @param mes           Número do mês.
     * @param codigoProduto Código do produto envolvido na transação.
     * @param quantidade    Quantidade transacionada.
     * @param preco         Preço do produto.
     * @param tipo          Regime da transação (Promoção / Normal).
     */
    public void update(String codigoCliente, int mes, String codigoProduto, int quantidade, double preco, String tipo) {
        // Mês
        if (this.registos.get(mes).containsKey(codigoCliente)) {
            this.registos.get(mes).get(codigoCliente).update(codigoProduto, tipo, quantidade, preco * quantidade);
        } else {
            ClienteProdutos cliprd = new ClienteProdutos(codigoCliente);
            cliprd.update(codigoProduto, tipo, quantidade, preco * quantidade);
            this.registos.get(mes).put(codigoCliente, cliprd);
        }

        // Anual (corresponde ao index 0)
        if (this.registos.get(0).containsKey(codigoCliente)) {
            this.registos.get(0).get(codigoCliente).update(codigoProduto, tipo, quantidade, preco * quantidade);
        } else {
            ClienteProdutos cliprd = new ClienteProdutos(codigoCliente);
            cliprd.update(codigoProduto, tipo, quantidade, preco * quantidade);
            this.registos.get(0).put(codigoCliente, cliprd);
        }
    }

    /**
     * Método que permite obter os códigos dos produtos que cada cliente comprou num dado mês.
     *
     * @param mes Número do mễs.
     * @return Associação entre cada cliente e os códigos dos produtos que adquiriu naquele mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public Map<String, Set<String>> getCodigosProdutosCompradosClientesMes(int mes) throws InvalidMonth {
        if (mes < 0 || mes > 12) throw new InvalidMonth("O Mês inserido não é válido!");
        Map<String, Set<String>> ret = new HashMap<>();
        Set<String> codigosProdutos;
        for (ClienteProdutos cp : this.registos.get(mes).values()) {
            codigosProdutos = cp.getProdutos().stream().map(InfoProduto::getCodigoProduto).collect(Collectors.toSet());
            ret.put(cp.getCodigoCliente(), codigosProdutos);
        }
        return ret;
    }

    /**
     * Método que permite obter o número de clientes que realizaram compras num dado mês.
     *
     * @param mes Número do mês.
     * @return Número de clientes que realizou compras no mês argumento.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public int getTotalClientesCompraramMes(int mes) throws InvalidMonth {

        if (mes < 1 || mes > 12) throw new InvalidMonth("O Mês inserido não é válido!");
        return registos.get(mes).size();
    }

    /**
     * Método que permite os códigos dos clientes que realizaram compras num dado mês.
     *
     * @param mes Número do mês.
     * @return Conjunto dos códigos de cliente que realizaram compras naquele mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public Set<String> getCodigosClientesMes(int mes) throws InvalidMonth {
        if (mes < 1 || mes > 12) throw new InvalidMonth("O Mês inserido não é válido!");
        Set<String> ret = this.registos.get(mes).values().stream().map(ClienteProdutos::getCodigoCliente).collect(Collectors.toSet());
        return ret;

    }

    /**
     * Método que permite determinar quais os produtos comprados por um dado cliente num dado mês.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Conjunto dos códigos dos produtos comprados por esse clientes nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public Set<String> getCodigosProdutosCompradosPorCliente(String codigo, int mes) throws InvalidMonth {
        if (mes < 1 || mes > 12) throw new InvalidMonth("O mês inserido não existe!");
        Set<String> produtosTotal = new HashSet<>();
        if (this.registos.get(mes).get(codigo) != null) {
            for (InfoProduto ip : this.registos.get(mes).get(codigo).getProdutos())
                produtosTotal.add(ip.getCodigoProduto());
        }
        return produtosTotal;
    }

    /**
     * Método que permite obter o total gasto por um cliente num dado mês.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Total gasto pelo cliente nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public double getTotalGastoClienteMes(String codigo, int mes) throws InvalidMonth {
        if (mes < 1 || mes > 12) throw new InvalidMonth("O mês inserido não existe!");
        if (this.registos.get(mes).get(codigo) != null)
            return this.registos.get(mes).get(codigo).getTotalFaturado();
        else return 0;
    }

    /**
     * Método que permite obter o número de compras realizadas num dado mês por um dado cliente.
     *
     * @param codigo Código do cliente a averiguar.
     * @param mes    Número do mês.
     * @return Número de compras realizadas por esse cliente nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public int getNumeroComprasClienteMes(String codigo, int mes) throws InvalidMonth {
        if (mes < 0 || mes > 12) throw new InvalidMonth("O mês inserido não existe!");
        if (this.registos.get(mes).containsKey(codigo))
            return this.registos.get(mes).get(codigo).getNumCompras();
        else return 0;
    }

    /**
     * Método que permite obter os códigos dos cliente que compraram um dado produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Conjunto dos códigos dos clientes que compraram aquele produto nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    public Set<String> getCodigosClientesCompraramProduto(String codigoProduto, int mes) throws InvalidMonth {
        if (mes < 0 || mes > 12) throw new InvalidMonth("O mês inserido não existe!");
        Set<String> produtosTotal = new HashSet<>();
        for (ClienteProdutos cp : this.registos.get(mes).values())
            if (cp.comprouProduto(codigoProduto)) produtosTotal.add(cp.getCodigoCliente());
        return produtosTotal;
    }

    /**
     * Método que permite obter o número de vezes que um determinado cliente comprou um determinado produto.
     *
     * @param codigoProduto   Código do produto a averiguar.
     * @param codigosClientes Código do cliente a averiguar.
     * @return Número de vezes que o cliente adquiriu aquele produto.
     */
    public Map<String, Integer> getNumeroComprasProdutoClientes(String codigoProduto, Set<String> codigosClientes) {
        Map<String, Integer> ret = new HashMap<>();
        for (String s : codigosClientes)
            ret.put(s, this.registos.get(0).get(s).getTotalComprasProduto(codigoProduto));
        return ret;
    }

    /**
     * Método que permite determinar o total faturado com um cliente e com um determinado produto.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param codigoCliente Código do cliente a averiguar.
     * @return Total faturado com o cliente a comprar o produto.
     */
    public double getTotalFaturadoProdutoCliente(String codigoProduto, String codigoCliente) {
        if (this.registos.get(0).containsKey(codigoCliente))
            return this.registos.get(0).get(codigoCliente).getTotalFaturadoProduto(codigoProduto);
        else return 0;
    }

    /**
     * Método que permite determinar quantas vezes um cliente comprou cada produto.
     *
     * @param codigoCliente Cliente a averiguar.
     * @return Associação entre os códigos de produto e o número de vezes que o cliente adquiriu cada um.
     */
    public Map<String, Integer> getQuantidadesProdutosCliente(String codigoCliente) {
        Map<String, Integer> produtosQ = new HashMap<>();
        for (InfoProduto ip : this.registos.get(0).get(codigoCliente).getProdutos())
            produtosQ.put(ip.getCodigoProduto(), ip.getQuantidadeTotal());
        return produtosQ;
    }

    /**
     * Método que permite obter uma lista de associações entre os 3 maiores compradores e total de dinheiro que gastaram.
     *
     * @return Lista (com 3 elementos) de associações entre o código de cliente dos maiores compradores e o total de
     * dinheiro dispendido.
     */
    public List<IParStringNum> get3MaioresCompradores() {
        Map<String, Double> compradores = new HashMap<>();
        for (ClienteProdutos cp : this.registos.get(0).values()) {
            String s = cp.getCodigoCliente();
            if (compradores.containsKey(s))
                compradores.put(s, compradores.get(s) + cp.getTotalFaturado());
            else compradores.put(s, cp.getTotalFaturado());
        }
        Set<IParStringNum> temp = new TreeSet<>(new CompareParValueString());
        for (Map.Entry<String, Double> par : compradores.entrySet())
            temp.add(new ParClienteFaturacao(par.getKey(), par.getValue()));
        Iterator<IParStringNum> it = temp.iterator();
        List<IParStringNum> ret = new ArrayList<>();
        int i = 0;
        while (it.hasNext() && i < 3) {
            ret.add(it.next());
            i++;
        }
        return ret;
    }


}
