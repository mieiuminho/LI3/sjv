package view;

import com.google.common.collect.Lists;
import common.IParStringNum;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.lang.System.out;

public class Navegador {
    private String title;

    private List<List<String>> pages;

    private List<List<IParStringNum>> rows;

    private List<String> header;

    private int current;

    private int size; // número de páginas: infelizmente não é só ver o tamaho de pages ou rows porque um deles é sempre null :(

    public static final int MAX_LINES_PAGE = 15;

    private static String[] opcoesMenuNavegacao = {
            "Página Seguinte",
            "Página Anterior",
            "Escolher página",
            "Sair",
    };

    public Navegador(Collection<String> collection, String title) {
        this.title = title;
        this.pages = Lists.partition(new ArrayList<>(collection), Navegador.MAX_LINES_PAGE);
        this.header = null;
        this.rows = null;
        this.current = 0;
        this.size = pages.size();
    }

    public Navegador(Collection<IParStringNum> collection, String title, List<String> header) {
        this.title = title;
        this.pages = null;
        this.header = new ArrayList<>(header);
        this.rows = Lists.partition(new ArrayList<>(collection), Navegador.MAX_LINES_PAGE);
        this.current = 0;
        this.size = rows.size();
    }

    public void show() {
        int option;
        do {
            Terminal.clear();
            GestVendasView.showLogo();
            if (this.header == null) {
                this.displayList(this.pages.get(current), this.title);
                out.println(StringUtils.center(
                        StringUtils.leftPad(" " + this.current + "/" + (this.pages.size() - 1), Terminal.TEXT_MAX_WIDTH - 1),
                        Terminal.WINDOW_WIDTH
                        )
                );
            } else {
                this.displayTable(this.rows.get(current), this.title);
                out.println(StringUtils.center(
                        StringUtils.leftPad(" " + this.current + "/" + (this.rows.size() - 1), Terminal.TEXT_MAX_WIDTH - 1),
                        Terminal.WINDOW_WIDTH
                        )
                );
            }
            new Menu("Opções de Navegação", opcoesMenuNavegacao).list();
            option = Input.lerOpcao(opcoesMenuNavegacao.length);
            switch (option) {
                case 1: // Página Seguinte
                    if (this.current + 1 < this.size) current++;
                    break;
                case 2: // Página Anterior
                    if (this.current > 0) current--;
                    break;
                case 3: // Escolher Página
                    this.current = Input.lerOpcao(this.size - 1);
                    break;
            }
        } while (option != 0);
    }

    public void displayList(List<String> lines, String title) {
        out.println(StringUtils.center(
                StringUtils.rightPad(" ╔", Terminal.TEXT_MAX_WIDTH, "═") + "╗ ", Terminal.WINDOW_WIDTH)
        );

        out.println(StringUtils.center(
                StringUtils.center(
                        StringUtils.center(title, Terminal.TEXT_MAX_WIDTH - 2), Terminal.TEXT_MAX_WIDTH, "║"
                ), Terminal.WINDOW_WIDTH
                )
        );

        for (String line : lines) {
            out.println(StringUtils.center(
                    StringUtils.rightPad("╠", Terminal.TEXT_MAX_WIDTH - 1, "═") + "╣", Terminal.WINDOW_WIDTH)
            );
            out.println(StringUtils.center(
                    StringUtils.rightPad("║ " + line, Terminal.TEXT_MAX_WIDTH - 2) + " ║", Terminal.WINDOW_WIDTH)
            );
        }

        out.println(StringUtils.center(
                StringUtils.rightPad(" ╚", Terminal.TEXT_MAX_WIDTH, "═") + "╝ ", Terminal.WINDOW_WIDTH)
        );
    }

    public void displayTable(List<IParStringNum> lines, String title) {
        int i, max_width;
        String row, separator;

        row = "";
        for (String field : this.header) row += " ║ " + field;
        max_width = row.length();

        out.println(StringUtils.center(
                StringUtils.rightPad(" ╔", max_width + 1, "═") + "╗", Terminal.WINDOW_WIDTH)
        );

        out.println(StringUtils.center(
                StringUtils.rightPad(" ║ " + title.toUpperCase(), max_width + 1) + "║", Terminal.WINDOW_WIDTH)
        );

        separator = " ╠";
        for (i = 0; i < this.header.size() - 1; i++) {
            for (int j = 0; j < this.header.get(i).length() + 2; j++) {
                separator += "═";
            }
            separator += "╦";
        }
        for (int j = 0; j < this.header.get(i).length() + 2; j++) {
            separator += "═";
        }
        out.println(StringUtils.center(
                StringUtils.rightPad(separator, max_width) + "╣", Terminal.WINDOW_WIDTH)
        );

        out.println(StringUtils.center(
                StringUtils.rightPad(row, max_width) + " ║", Terminal.WINDOW_WIDTH)
        );

        for (IParStringNum line : lines) {
            separator = " ╠";
            for (i = 0; i < this.header.size() - 1; i++) {
                for (int j = 0; j < this.header.get(i).length() + 2; j++) {
                    separator += "═";
                }
                separator += "╬";
            }
            for (int j = 0; j < this.header.get(i).length() + 2; j++) {
                separator += "═";
            }
            out.println(StringUtils.center(
                    StringUtils.rightPad(separator, max_width) + "╣", Terminal.WINDOW_WIDTH)
            );

            row = "";
            row += StringUtils.rightPad(" ║ " + line.getString(), this.header.get(0).length() + 3);
            row += " ║ " + StringUtils.leftPad("" + line.getNumber().intValue(), this.header.get(1).length());

            out.println(StringUtils.center(
                    StringUtils.rightPad(row, max_width) + " ║", Terminal.WINDOW_WIDTH)
            );
        }

        separator = " ╚";
        for (i = 0; i < this.header.size() - 1; i++) {
            for (int j = 0; j < this.header.get(i).length() + 2; j++) {
                separator += "═";
            }
            separator += "╩";
        }
        for (int j = 0; j < this.header.get(i).length() + 2; j++) {
            separator += "═";
        }

        out.println(StringUtils.center(
                StringUtils.rightPad(separator, max_width) + "╝", Terminal.WINDOW_WIDTH)
        );
    }

    public void displayQuickTable() {
        this.displayTable(this.rows.get(current), this.title);
    }
}
