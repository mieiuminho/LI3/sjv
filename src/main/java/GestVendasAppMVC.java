import controller.GestVendasController;
import controller.IGestVendasController;
import model.GestVendasModel;
import model.IGestVendasModel;
import view.GestVendasView;
import view.IGestVendasView;

public class GestVendasAppMVC {

    public static void main(String[] args) {
        IGestVendasModel model = new GestVendasModel();
        IGestVendasView view = new GestVendasView();
        IGestVendasController controller = new GestVendasController(model, view);
        controller.run();
    }
}
