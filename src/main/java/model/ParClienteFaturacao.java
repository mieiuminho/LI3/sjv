package model;

import common.IParStringNum;

import java.io.Serializable;

public class ParClienteFaturacao implements Serializable, IParStringNum {
    private String codigoCliente;
    private double faturacao;

    public ParClienteFaturacao(String codigoCliente, double faturacao) {
        this.codigoCliente = codigoCliente;
        this.faturacao = faturacao;
    }

    @Override
    public void setString(String string) {
        this.codigoCliente = string;
    }

    @Override
    public String getString() {
        return this.codigoCliente;
    }

    @Override
    public Number getNumber() {
        return this.faturacao;
    }
}
