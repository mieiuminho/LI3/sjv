package model;

import common.IParStringNum;
import exceptions.InvalidCliente;
import exceptions.InvalidFilial;
import exceptions.InvalidMonth;
import exceptions.InvalidProduto;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface IGestVendasModel {

    /**
     * Método que permite gravar o estado do programa através de uma stream de objetos.
     *
     * @throws IOException Caso a diretoria não exista.
     */
    void save() throws IOException;

    /**
     * Método que permite recuperar os dados gravados sob a forma de uma stream de objetos para o ficheiro que está em
     * 'DATABASE_PATH'.
     *
     * @return Objeto do tipo `GestVendasModel`
     * @throws ClassNotFoundException Caso ocorra um erro no "cast".
     * @throws IOException            Caso o ficheiro não exista.
     */
    IGestVendasModel load() throws ClassNotFoundException, IOException;

    /**
     * Método que permite criar a base de dados.
     *
     * @param ficheiroClientes Caminho para o ficheiro de Clientes.
     * @param ficheiroProdutos Caminho para o ficheiro de Produtos.
     * @param ficheiroVendas   Caminho para o ficheiro de Vendas.
     */
    void createData(String ficheiroClientes, String ficheiroProdutos, String ficheiroVendas);

    /**
     * Método que permite obter o último ficheiro de vendas lido.
     *
     * @return Último ficheiro de vendas lido.
     */
    String getUltimoFicheiroLido();

    /**
     * Método que permite obter os códigos dos produtos não transacionados.
     *
     * @return Conjunto dos códigos dos produtos não transacionados.
     */
    Set<String> getCodigosProdutosNaoComprados();

    /**
     * Método que permite obter o número de filiais.
     *
     * @return Número de filiais.
     */
    int getNumFiliais();

    /**
     * Método que permite obter o número total de vendas num determindao mês.
     *
     * @param mes Número do mês.
     * @return Número total de vendas nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    int getTotalVendasMes(int mes) throws InvalidMonth;

    /**
     * Método que permite obter o número total de vendas num determinado mês numa filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número total de vendas nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    int getTotalVendasMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial;

    /**
     * Método que permite obter o total de clientes que fizeram compras num mês.
     *
     * @param mes Número do mês.
     * @return Número de clientes que fizeram compras nesse mês.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    int getTotalClientesCompraramMes(int mes) throws InvalidMonth;

    /**
     * Método que permite obter o número de clientes que fizeram compras numa determinada filial num determinado mês.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Número de clientes que realizaram compras nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    int getTotalClientesCompraramMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial;

    /**
     * Método que permite obter o número de produtos diferentes que um cliente comprou num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Número de produtos diferentes que esse cliente comprou nesse mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    int getProdutosDiferentesMesCliente(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth;

    /**
     * Método que permite determinar o montante gasto por um cliente num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Montante gasto pelo cliente naquele mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    double totalGastoClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth;

    /**
     * Método que permite obter o total de compras realizadas por um cliente num dado mês.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param mes           Número do mês.
     * @return Número de compras realizadas pelo cliente naquele mês.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    int totalComprasClienteMes(String codigoCliente, int mes) throws InvalidCliente, InvalidMonth;

    /**
     * Método que permite determinar por quantos clientes diferentes foi adquirido um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Número de clientes diferentes que adquiriram o produto naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    int getClientesDiferentesMesProduto(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth;

    /**
     * Método que permite determinar quantas vezes foi transacionado um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Número de vezes que o produto foi transacionado naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    int getComprasProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth;

    /**
     * Método que permite obter a faturação gerada por um produto num dado mês.
     *
     * @param codigoProduto Código do produto a averiguar.
     * @param mes           Número do mês.
     * @return Faturação gerada pelo produto naquele mês.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    double getFaturacaoProdutoMes(String codigoProduto, int mes) throws InvalidProduto, InvalidMonth;

    /**
     * Método que permite determinar a quantidade que um cliente adquiriu de cada produto.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @return Associação entre cada código de produto e a quantidade adquirida do mesmo pelo cliente.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     */
    Set<IParStringNum> getProdutosPorQuantidadeCliente(String codigoCliente) throws InvalidCliente;

    /**
     * Método que permite obter os X produtos mais vendidos.
     *
     * @param x Número de produtos que prentendemos ordenar em ordem à quantidade vendida.
     * @return Lista que associa cada um dos top X produtos à quantidade vendida dos mesmos.
     */
    List<IParStringNum> getXProdutosMaisVendidosTotal(int x);

    /**
     * Método que permite obter os 3 maiores compradores de cada filial.
     *
     * @return Lista de lista que associa cada um dos compradores à quantidade comprada.
     */
    List<List<IParStringNum>> get3MaioresCompradoresFiliais();

    /**
     * Método que permite obter a lista dos X clientes que compraram mais produtos diferentes.
     *
     * @param x Número de clientes que queremos ordenar em ordem à quantidade de produtos diferentes que adquiriram.
     * @return Lista que associa cada cliente do top X à quantidade de produtos diferentes adquiridos.
     * @throws InvalidMonth Caso o mês argumento seja inválido.
     */
    List<IParStringNum> getXClientesMaisProdutosDiferentes(int x) throws InvalidMonth;

    /**
     * Método que permite obter a lista dos X clientes que mais compraram um dado produto.
     *
     * @param codigo Código do produto a averiguar.
     * @param x      Número de clientes a ordenar em ordem à quantidade comprada do produto dado.
     * @return Lista que associa cada cliente do top X à quantidade comprada do produto dado.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidMonth   Caso o mês argumento seja inválido.
     */
    List<IParStringNum> getXClientesMaisCompraramProduto(String codigo, int x) throws InvalidProduto, InvalidMonth;

    /**
     * Método que permite obter o total gasto por um cliente num dado produto.
     *
     * @param codigoCliente Código do cliente a averiguar.
     * @param codigoProduto Código do produto a averiguar.
     * @return Montante gasto pelo cliente naquele produto.
     * @throws InvalidProduto Caso o produto argumento não se encontre registado.
     * @throws InvalidCliente Caso o cliente argumento não se encontre registado.
     */
    double getTotalGastoClienteProduto(String codigoCliente, String codigoProduto) throws InvalidProduto, InvalidCliente;

    /**
     * Método que permite obter o número total de clientes registados.
     *
     * @return Número total de clientes registados.
     */
    int getTotalClientes();

    /**
     * Método que permite obter o número total de produtos registados.
     *
     * @return Número total de produtos registados.
     */
    int getTotalProdutos();

    /**
     * Método que permite obter o número total de produtos não transacionados.
     *
     * @return Número total de produtos não transacionados.
     */
    int getTotalProdutosNaoComprados();

    /**
     * Método que permite obter o número total de clientes que efetuaram compras.
     *
     * @return Número total de clientes que efetuaram compras.
     */
    int getTotalClientesCompraram();

    /**
     * Método que permite obter o número de vendas erradas.
     *
     * @return Número de vendas erradas.
     */
    int getNrVendasErradas();

    /**
     * Método que permite obter o total faturado num determinado mês numa determinada filial.
     *
     * @param mes    Número do mês.
     * @param filial Número da filial.
     * @return Total faturado nesse mês nessa filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    double getTotalFaturadoMesFilial(int mes, int filial) throws InvalidMonth, InvalidFilial;

    /**
     * Método que permite obter o total faturado.
     *
     * @return Total faturado.
     */
    double getTotalFaturado();

    /**
     * Método que permite obter a faturação de cada produto por mês e por filial.
     *
     * @return Associação da faturação de cada produto por mês e por filial.
     * @throws InvalidMonth  Caso o mês argumento seja inválido.
     * @throws InvalidFilial Caso a filial argumento seja inválida.
     */
    List<List<List<IParStringNum>>> getFaturacaoProdutosMesFilial() throws InvalidMonth, InvalidFilial;

}
